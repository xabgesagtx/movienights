ARG BUILD_HOME=/build-home

## build npm
FROM node:14 as build-npm-image
ARG BUILD_HOME
ENV APP_HOME=$BUILD_HOME
COPY frontend $APP_HOME
WORKDIR $APP_HOME

RUN npm install
RUN npm run build

## build gradle
FROM gradle:6.7-jdk11 as build-gradle-image

ARG BUILD_HOME
ENV APP_HOME=$BUILD_HOME
WORKDIR $APP_HOME

COPY --chown=gradle:gradle build.gradle.kts settings.gradle.kts $APP_HOME/
COPY --chown=gradle:gradle src $APP_HOME/src
COPY --from=build-npm-image --chown=gradle:gradle $APP_HOME/dist/frontend $APP_HOME/frontend/dist/frontend

RUN gradle -PskipNode --no-daemon build -x test


## build final container
FROM eclipse-temurin:11-alpine

ARG BUILD_HOME
ARG CA_CERT
ENV APP_HOME=$BUILD_HOME
ENV JAVA_OPTS=""

RUN echo "$CA_CERT" > /ca-certificate.crt
RUN keytool -cacerts -storepass changeit -noprompt -trustcacerts -importcert -alias mongo -file /ca-certificate.crt
RUN rm /ca-certificate.crt

COPY --from=build-gradle-image $APP_HOME/build/libs/movienights.jar app.jar

ENTRYPOINT java $JAVA_OPTS -jar app.jar