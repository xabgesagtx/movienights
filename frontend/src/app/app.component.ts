import {Component} from '@angular/core';
import {AuthService} from './auth/auth.service';
import * as moment from 'moment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private authService: AuthService) {
    this.authService.configure();
    AppComponent.configureMoment();
  }

  private static configureMoment() {
    moment.locale('en', {
      calendar: {
        lastDay: '[yesterday at] HH:mm',
        sameDay: '[today at] HH:mm',
        nextDay: '[tomorrow at] HH:mm',
        lastWeek: '[last] dddd [at] HH:mm',
        nextWeek: 'dddd [at] HH:mm',
        sameElse: 'DD.MM.YYYY'
      }
    });
  }
}
