import {UsernamePipe} from './username.pipe';
import {inject, TestBed, waitForAsync} from '@angular/core/testing';
import {AuthService} from './auth.service';

describe('UsernamePipe', () => {
  const authServiceSpy = jasmine.createSpyObj('AuthService', ['currentUser']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: AuthService, useValue: authServiceSpy }
      ]
    });
  }));
  it('create an instance', inject([AuthService], (authService: AuthService) => {
    const pipe = new UsernamePipe(authService);
    expect(pipe).toBeTruthy();
  }));
});
