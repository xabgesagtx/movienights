import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {faSignInAlt, faSignOutAlt} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  faSignOutAlt = faSignOutAlt;
  faSignInAlt = faSignInAlt;

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  login() {
    this.authService.login();
  }

  logout() {
    this.authService.logout();
  }

  get username() {
    return this.authService.currentUser.name;
  }

  get isAdmin() {
    return this.authService.currentUser.isAdmin;
  }

  get loggedIn() {
    return this.authService.loggedIn;
  }

}
