import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from './auth.service';
import {MessageService} from '../shared/message.service';
import {filter, map} from 'rxjs/operators';

@Injectable()
export class LoggedInGuard implements CanActivate {

  constructor(private authService: AuthService, private messageService: MessageService, private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.authService.isDoneLoading$
      .pipe(filter(isDone => isDone))
      .pipe(map(() => {
        if (this.authService.loggedIn) {
          return true;
        } else {
          this.messageService.showError('You have to login!');
          return this.router.createUrlTree(['/']);
        }
      }));

  }

}
