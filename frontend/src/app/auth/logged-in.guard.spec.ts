import {inject, TestBed} from '@angular/core/testing';

import {LoggedInGuard} from './logged-in.guard';
import {AuthService} from './auth.service';
import {MessageService} from '../shared/message.service';
import {RouterTestingModule} from '@angular/router/testing';

describe('LoggedInGuard', () => {
  const authServiceSpy = jasmine.createSpyObj('AuthService', ['login']);
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError']);
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      providers: [
        LoggedInGuard,
        { provide: AuthService, useValue: authServiceSpy},
        { provide: MessageService, useValue: messageServiceSpy},
      ]
    });
  });

  it('should ...', inject([LoggedInGuard], (guard: LoggedInGuard) => {
    expect(guard).toBeTruthy();
  }));
});
