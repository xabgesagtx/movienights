import {Pipe, PipeTransform} from '@angular/core';
import {AuthService} from './auth.service';

@Pipe({
  name: 'username'
})
export class UsernamePipe implements PipeTransform {

  constructor(private authService: AuthService) {

  }

  transform(value: { id: string, name: string}): string {
    const currentUser = this.authService.currentUser;
    if (currentUser && value.id === currentUser.id) {
      return 'you';
    } else {
      return value.name;
    }
  }

}
