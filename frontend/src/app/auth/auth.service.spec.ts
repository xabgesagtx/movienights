import {TestBed} from '@angular/core/testing';

import {AuthService} from './auth.service';
import {OAuthService} from 'angular-oauth2-oidc';
import {UserService} from './user.service';

describe('AuthService', () => {
  const oAuthServiceSpy = jasmine.createSpyObj('OAuthService', ['getClaim']);
  const userServiceSpy = jasmine.createSpyObj('UserService', ['register']);
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      AuthService,
      {provide: OAuthService, useValue: oAuthServiceSpy},
      {provide: UserService, useValue: userServiceSpy},
    ]
  }));

  it('should be created', () => {
    const service: AuthService = TestBed.inject(AuthService);
    expect(service).toBeTruthy();
  });
});
