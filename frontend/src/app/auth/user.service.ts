import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from './model/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  register(id: string, name: string, email: string) {
    return this.httpClient.post('/api/users/register', { id, name, email });
  }

  findAll() {
    return this.httpClient.get<User[]>('/api/users');
  }
}
