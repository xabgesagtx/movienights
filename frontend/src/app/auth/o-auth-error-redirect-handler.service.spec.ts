import {TestBed} from '@angular/core/testing';

import {OAuthErrorRedirectHandler} from './o-auth-error-redirect-handler.service';
import {AuthService} from './auth.service';
import {RouterTestingModule} from '@angular/router/testing';
import {MessageService} from '../shared/message.service';

describe('OAuthErrorRedirectHandlerService', () => {
  const authServiceSpy = jasmine.createSpyObj('AuthService', ['login']);
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError']);

  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      RouterTestingModule
    ],
    providers: [
      OAuthErrorRedirectHandler,
      {provide: AuthService, useValue: authServiceSpy},
      {provide: MessageService, useValue: messageServiceSpy},
    ]
  }));

  it('should be created', () => {
    const service: OAuthErrorRedirectHandler = TestBed.inject(OAuthErrorRedirectHandler);
    expect(service).toBeTruthy();
  });
});
