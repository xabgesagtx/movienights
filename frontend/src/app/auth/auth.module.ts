import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OAuthModule, OAuthResourceServerErrorHandler, OAuthStorage} from 'angular-oauth2-oidc';
import {OAuthErrorRedirectHandler} from './o-auth-error-redirect-handler.service';
import {AuthService} from './auth.service';
import {LoggedInGuard} from './logged-in.guard';
import {LoginComponent} from './login/login.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {UsernamePipe} from './username.pipe';
import {UserService} from './user.service';


export function storageFactory(): OAuthStorage {
  return localStorage;
}

@NgModule({
  declarations: [LoginComponent, UsernamePipe],
  imports: [
    CommonModule,
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: ['/api'],
        sendAccessToken: true
      }
    }),
    FontAwesomeModule,
  ],
  exports: [
    LoginComponent,
    UsernamePipe
  ],
  providers: [
    AuthService,
    UserService,
    LoggedInGuard,
    {provide: OAuthResourceServerErrorHandler, useClass: OAuthErrorRedirectHandler},
    {provide: OAuthStorage, useFactory: storageFactory}
  ]
})
export class AuthModule { }
