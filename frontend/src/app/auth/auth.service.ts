import {Injectable, NgZone} from '@angular/core';
import {NullValidationHandler, OAuthService} from 'angular-oauth2-oidc';
import {authConfig} from './auth.config';
import {ReplaySubject} from 'rxjs';
import {filter} from 'rxjs/operators';
import {UserService} from './user.service';
import {User} from './model/User';
import {HttpErrorResponse} from '@angular/common/http';

@Injectable()
export class AuthService {

  private isDoneLoadingSubject$ = new ReplaySubject<boolean>();
  public isDoneLoading$ = this.isDoneLoadingSubject$.asObservable();
  private isRegistered = false;

  constructor(private zone: NgZone, private oAuthService: OAuthService, private userService: UserService) { }

  configure() {
    this.oAuthService.configure(authConfig);
    this.oAuthService.tokenValidationHandler = new NullValidationHandler();

    this.oAuthService.events
      .pipe(filter(e => ['token_received'].includes(e.type)))
      .subscribe(() => {
        if (!this.isRegistered) {
          const {id, name, email} = this.currentUser;
          this.userService.register(id, name, email).subscribe(() => this.isRegistered = true);
        }
      });
    this.oAuthService.loadDiscoveryDocument()
      .then(() => this.initialLogin());
    this.oAuthService.setupAutomaticSilentRefresh();


    this.zone.runOutsideAngular(() => {
      setInterval(() => {
        if (!this.oAuthService.hasValidAccessToken() && this.oAuthService.getRefreshToken()) {
          this.zone.run(() => this.oAuthService.refreshToken()
            .catch((err: HttpErrorResponse) => this.handleRefreshTokenError(err))
            .then(() => this.oAuthService.setupAutomaticSilentRefresh())
          );
        }
      }, 3000);

    });


  }

  private initialLogin(): Promise<any> {
    if (this.oAuthService.getRefreshToken()) {
      return this.oAuthService.refreshToken()
        .catch((err: HttpErrorResponse) => this.handleRefreshTokenError(err))
        .finally(() => this.isDoneLoadingSubject$.next(true));
    } else {
      return this.oAuthService.tryLogin().finally(() => this.isDoneLoadingSubject$.next(true));
    }
  }

  get currentUser(): User {
    return {
      id: this.getClaim('sub'),
      name: this.getClaim('name'),
      email: this.getClaim('email'),
      isAdmin: this.isAdmin()
    };
  }

  private getClaim(claim: string) {
    const claims: any = this.oAuthService.getIdentityClaims();
    if (!claims) { return null; }

    return claims[claim];
  }

  private isAdmin() {
    const scopes: any = this.oAuthService.getGrantedScopes();
    if (scopes && scopes.length > 0) {
      return scopes[0].split(' ').indexOf('movienights_admin') !== -1;
    } else {
      return false;
    }
  }

  get loggedIn() {
    return this.oAuthService.hasValidIdToken() &&
      this.oAuthService.hasValidAccessToken();
  }

  login() {
    this.oAuthService.initLoginFlow();
  }

  logout() {
    this.isRegistered = false;
    this.oAuthService.revokeTokenAndLogout({
      client_id: this.oAuthService.clientId,
      returnTo: this.oAuthService.redirectUri
    }, true);
  }

  private handleRefreshTokenError(err: HttpErrorResponse) {
    if (err.status >= 400 && err.status < 500) {
      console.log(`Failed to refresh token: ${err.status} - ${err.message}. Logging out`);
      this.logout();
    } else {
      console.log(`Failed to refresh token: ${err.status} - ${err.message}.`);
    }
  }
}
