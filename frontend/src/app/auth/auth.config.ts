import {AuthConfig} from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {

  issuer: 'https://dev-canceledsystems.eu.auth0.com/',

  redirectUri: window.location.origin + '/',

  clientId: 'juAehzMaQmqsTvF17eQWIqr8FdjSqUJM',

  scope: 'openid profile email offline_access movienights movienights_admin',

  logoutUrl: 'https://dev-canceledsystems.eu.auth0.com/v2/logout',

  responseType: 'code',

  customQueryParams: {
    audience: 'movienights'
  }

};
