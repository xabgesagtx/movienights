import {Video} from './video';
import {User} from './user';

export class Movie {
  id: string;
  createdAt: string;
  createdBy: User;
  externalId: number;
  watchedBy: User[];
  title: string;
  year?: number;
  adult: boolean;
  duration?: boolean;
  poster: string;
  description?: string;
  imdbId?: string;
  language: string;
  originalTitle: string;
  countries: string[];
  videos: Video[];
  genres: string[];
  likes: User[];
}
