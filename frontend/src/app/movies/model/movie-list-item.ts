import {User} from './user';

export class MovieListItem {
  id: string;
  createdAt: string;
  createdBy: User;
  watchedBy: User[];
  title: string;
  year?: number;
  adult: boolean;
  poster: string;
  language: string;
  originalTitle: string;
  likes: User[];
  numberOfComments: number;
}
