import {Component, OnInit} from '@angular/core';
import {MoviesService} from '../movies.service';
import {MessageService} from '../../shared/message.service';
import {HttpErrorResponse} from '@angular/common/http';
import {MovieListItem} from '../model/movie-list-item';
import {ActivatedRoute} from '@angular/router';
import {User} from '../model/user';
import {AuthService} from '../../auth/auth.service';
import {IDropdownSettings} from 'ng-multiselect-dropdown';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent implements OnInit {

  loading = true;
  movies: MovieListItem[] = [];
  users: User[] = [];
  watchedFilter = 'unwatched';
  usersFilter: User[] = [];
  sortBy = 'createdAt';
  multiselectSettings: IDropdownSettings = {
    idField: 'id',
    textField: 'name',
    itemsShowLimit: 5
  };

  constructor(private movieService: MoviesService,
              private authService: AuthService,
              private messageService: MessageService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.subscribe((data: { users: User[] }) => {
      this.users = data.users;
    });
    const watchedFilter = localStorage.getItem('watchedFilter');
    if (watchedFilter) {
      this.watchedFilter = watchedFilter;
    }
    const usersFilter = localStorage.getItem('usersFilter');
    if (usersFilter) {
      this.usersFilter = JSON.parse(usersFilter);
    } else if (this.authService.currentUser) {
      this.usersFilter = [this.authService.currentUser];
    }
    const sortBy = localStorage.getItem('sortBy');
    if (sortBy) {
      this.sortBy = sortBy;
    }
    this.movieService.findAll()
      .subscribe(movies => {
        this.movies = movies;
        this.loading = false;
      }, (error: HttpErrorResponse) => {
        this.messageService.showError(`Failed loading movies: ${error.status} - ${error.message}`);
        this.loading = false;
      });
  }

  removeMovie(movieId: string) {
    const index = this.movies.findIndex(movie => movie.id === movieId);
    this.movies.splice(index, 1);
  }

  get moviesSorted() {
    return this.movies.filter(movie => this.filter(movie))
      .sort((movieA, movieB) => this.sort(movieA, movieB));
  }

  filterChanged() {
    localStorage.setItem('watchedFilter', this.watchedFilter);
  }

  usersFilterChanged() {
    localStorage.setItem('usersFilter', JSON.stringify(this.usersFilter));
  }

  sortByChanged() {
    localStorage.setItem('sortBy', this.sortBy);
  }

  filter(movie: MovieListItem) {
    if (this.usersFilter.length === 0) {
      return true;
    } else if (this.watchedFilter === 'unwatched') {
      return !this.usersFilter.some(filterUser => movie.watchedBy.some(watchUser => watchUser.id === filterUser.id));
    } else if (this.watchedFilter === 'watched') {
      return this.usersFilter.every(filterUser => movie.watchedBy.some(watchUser => watchUser.id === filterUser.id));
    } else {
      return true;
    }
  }

  sort(movieA: MovieListItem, movieB: MovieListItem) {
    if (this.sortBy === 'createdAt') {
      return movieB.createdAt.localeCompare(movieA.createdAt);
    } else if (this.sortBy === 'title') {
      return movieA.title.localeCompare(movieB.title);
    } else {
      const compareLikes = movieB.likes.length - movieA.likes.length;
      return compareLikes === 0 ? movieA.title.localeCompare(movieB.title) : compareLikes;
    }
  }

}
