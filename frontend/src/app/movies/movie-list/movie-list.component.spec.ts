import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {MovieListComponent} from './movie-list.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {MessageService} from '../../shared/message.service';
import {MoviesService} from '../movies.service';
import {of} from 'rxjs';
import {AuthService} from '../../auth/auth.service';
import {RouterTestingModule} from '@angular/router/testing';

describe('MovieListComponent', () => {
  let component: MovieListComponent;
  let fixture: ComponentFixture<MovieListComponent>;
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError']);
  const moviesServiceSpy = jasmine.createSpyObj('MoviesService', {
    findAll: of([])
  });
  const authServiceSpy = jasmine.createSpyObj('AuthService', ['currentUser']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        MovieListComponent
      ],
      providers: [
        { provide: MessageService, useValue: messageServiceSpy },
        { provide: MoviesService, useValue: moviesServiceSpy },
        { provide: AuthService, useValue: authServiceSpy }
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
