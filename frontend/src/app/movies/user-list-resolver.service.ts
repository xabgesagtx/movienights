import {Injectable} from '@angular/core';
import {User} from './model/user';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {EMPTY, Observable} from 'rxjs';
import {UserService} from '../auth/user.service';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserListResolverService implements Resolve<User[]>{

  constructor(private userService: UserService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User[]> | Promise<User[]> | User[] {
    return this.userService.findAll().pipe(
      map(response => {
        if (response) {
          return response;
        } else {
          return [];
        }
      }),
      catchError(() => {
        this.router.navigate(['/some-error']);
        return EMPTY as Observable<User[]>;
      })
    );
  }
}
