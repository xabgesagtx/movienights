import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MovieListComponent} from './movie-list/movie-list.component';
import {MovieListItemComponent} from './movie-list-item/movie-list-item.component';
import {MovieDetailsComponent} from './movie-details/movie-details.component';
import {MoviesService} from './movies.service';
import {MovieDetailsResolverService} from './movie-details-resolver.service';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../shared/shared.module';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {MomentModule} from 'ngx-moment';
import {LikeComponent} from './like/like.component';
import {WatchComponent} from './watch/watch.component';
import {FormsModule} from '@angular/forms';
import {RemoveComponent} from './remove/remove.component';
import {CommentsModule} from '../comments/comments.module';
import {AuthModule} from '../auth/auth.module';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';


@NgModule({
  declarations: [
    MovieListComponent,
    MovieListItemComponent,
    MovieDetailsComponent,
    LikeComponent,
    WatchComponent,
    RemoveComponent
  ],
    imports: [
        CommonModule,
        RouterModule,
        SharedModule,
        FontAwesomeModule,
        MomentModule,
        FormsModule,
        CommentsModule,
        AuthModule,
        NgMultiSelectDropDownModule
    ],
  providers: [
    MoviesService,
    MovieDetailsResolverService
  ]
})
export class MoviesModule {
}
