import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {LikeComponent} from './like.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {MoviesService} from '../movies.service';
import {AuthService} from '../../auth/auth.service';
import {MessageService} from '../../shared/message.service';

describe('LikeComponent', () => {
  let component: LikeComponent;
  let fixture: ComponentFixture<LikeComponent>;
  const authServiceSpy = jasmine.createSpyObj('AuthService', ['login']);
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError']);
  const movieServiceSpy = jasmine.createSpyObj('MoviesService', ['like', 'unlike']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        LikeComponent
      ],
      providers: [
        {provide: AuthService, useValue: authServiceSpy},
        {provide: MessageService, useValue: messageServiceSpy},
        {provide: MoviesService, useValue: movieServiceSpy},
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LikeComponent);
    component = fixture.componentInstance;
    component.movieId = 'id';
    component.likes = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
