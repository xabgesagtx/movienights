import {Component, HostBinding, HostListener, Input, OnInit} from '@angular/core';
import {MoviesService} from '../movies.service';
import {AuthService} from '../../auth/auth.service';
import {MessageService} from '../../shared/message.service';
import {HttpErrorResponse} from '@angular/common/http';
import {User} from '../model/user';
import {faThumbsUp} from '@fortawesome/free-solid-svg-icons';

@Component({
  // tslint:disable-next-line:component-selector
  selector: '[app-like]',
  templateUrl: './like.component.html',
  styleUrls: ['./like.component.scss'],
  preserveWhitespaces: true
})
export class LikeComponent implements OnInit {

  faThumbsUp = faThumbsUp;

  @Input()
  movieId: string;

  @Input()
  likes: User[];

  constructor(private movieService: MoviesService, private authService: AuthService, private messageService: MessageService) { }

  ngOnInit() {
  }

  @HostBinding('class') get classValue() {
    if (this.liked) {
      return 'btn btn-secondary';
    } else {
      return 'btn btn-outline-secondary';
    }
  }

  get liked(): boolean {
    return this.likes.filter(elem => elem.id === this.authService.currentUser.id).length > 0;
  }

  @HostBinding('title') get likeText() {
    let users;
    if (this.likes.length === 0) {
      users = 'no one';
    } else {
      users = this.likes.map(user => user.name).join(', ');
    }
    return `Liked by ${users}`;
  }

  @HostListener('click') toggleLike() {
    const currentUser = this.authService.currentUser;
    if (this.liked) {
      this.movieService.unlike(this.movieId).subscribe(() => {
        const index: number = this.likes.findIndex(user => user.id === currentUser.id);
        if (index !== -1) {
          this.likes.splice(index, 1);
        }
      }, (error: HttpErrorResponse) => this.messageService.showError(`Failed to unlike: ${error.status} - ${error.message}`));
    } else {
      this.movieService.like(this.movieId).subscribe(() => this.likes.push(currentUser),
        (error: HttpErrorResponse) => this.messageService.showError(`Failed to like: ${error.status} - ${error.message}`));
    }
  }

}
