import {Component, HostBinding, HostListener, Input, OnInit} from '@angular/core';
import {MoviesService} from '../movies.service';
import {HttpErrorResponse} from '@angular/common/http';
import {MessageService} from '../../shared/message.service';
import {AuthService} from '../../auth/auth.service';
import {User} from '../model/user';
import {faEye} from '@fortawesome/free-solid-svg-icons';

@Component({
  // tslint:disable-next-line:component-selector
  selector: '[app-watch]',
  templateUrl: './watch.component.html',
  styleUrls: ['./watch.component.scss'],
  preserveWhitespaces: true
})
export class WatchComponent implements OnInit {

  @Input()
  watchedBy: User[];

  @Input()
  movieId: string;

  faEye = faEye;

  constructor(private moviesService: MoviesService, private authService: AuthService, private messageService: MessageService) {
  }

  ngOnInit() {
  }

  @HostBinding('class') get classValue() {
    if (this.watched) {
      return 'btn btn-secondary';
    } else {
      return 'btn btn-outline-secondary';
    }
  }

  get watched(): boolean {
    return this.watchedBy.some(elem => elem.id === this.authService.currentUser.id);
  }

  @HostBinding('title') get watchText() {
    let users;
    if (this.watchedBy.length === 0) {
      users = 'no one';
    } else {
      users = this.watchedBy.map(user => user.name).join(', ');
    }
    return `Watched by ${users}`;
  }

  @HostListener('click') toggleWatch() {
    const currentUser = this.authService.currentUser;
    if (this.watched) {
      this.moviesService.unwatch(this.movieId).subscribe(() => {
        const index: number = this.watchedBy.findIndex(user => user.id === currentUser.id);
        if (index !== -1) {
          this.watchedBy.splice(index, 1);
        }
      }, (error: HttpErrorResponse) => this.messageService.showError(`Failed to unlike: ${error.status} - ${error.message}`));
    } else {
      this.moviesService.watch(this.movieId).subscribe(() => this.watchedBy.push(currentUser),
        (error: HttpErrorResponse) => this.messageService.showError(`Failed to unlike: ${error.status} - ${error.message}`));
    }
  }

}
