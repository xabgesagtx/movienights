import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {WatchComponent} from './watch.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {MessageService} from '../../shared/message.service';
import {MoviesService} from '../movies.service';
import {AuthService} from '../../auth/auth.service';

describe('WatchComponent', () => {
  let component: WatchComponent;
  let fixture: ComponentFixture<WatchComponent>;
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError']);
  const moviesServiceSpy = jasmine.createSpyObj('MoviesService', ['watch', 'unwatch']);
  const authServiceSpy = jasmine.createSpyObj('AuthService', ['currentUser']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        WatchComponent
      ],
      providers: [
        {provide: MessageService, useValue: messageServiceSpy},
        {provide: MoviesService, useValue: moviesServiceSpy},
        {provide: AuthService, useValue: authServiceSpy}
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WatchComponent);
    component = fixture.componentInstance;
    component.watchedBy = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
