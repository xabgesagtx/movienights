import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Movie} from './model/movie';
import {EMPTY, Observable} from 'rxjs';
import {catchError, map, take} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';
import {MoviesService} from './movies.service';

@Injectable()
export class MovieDetailsResolverService implements Resolve<Movie> {

  constructor(private router: Router, private moviesService: MoviesService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Movie> | Promise<Movie> | Movie {
    const id = route.paramMap.get('id');
    return this.moviesService.findById(id).pipe(
      take(1),
      map(response => {
        if (response) {
          return response;
        } else {
          this.router.navigate(['/not-found']);
          return null;
        }
      }),
      catchError((err: HttpErrorResponse) => {
        if (err.status === 404) {
          this.router.navigate(['/not-found']);
        } else {
          this.router.navigate(['/some-error']);
        }
        return EMPTY as Observable<Movie>;
      })
    );
  }
}
