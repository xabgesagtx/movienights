import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {RemoveComponent} from './remove.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MoviesService} from '../movies.service';
import {MessageService} from '../../shared/message.service';

describe('RemoveComponent', () => {
  let component: RemoveComponent;
  let fixture: ComponentFixture<RemoveComponent>;
  const moviesServiceSpy = jasmine.createSpyObj('MoviesService', ['remove']);
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError', 'showSuccess']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        NgbModule
      ],
      declarations: [ RemoveComponent ],
      providers: [
        { provide: MoviesService, useValue: moviesServiceSpy },
        { provide: MessageService, useValue: messageServiceSpy },
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
