import {Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild} from '@angular/core';
import {faTrashAlt} from '@fortawesome/free-solid-svg-icons';
import {HttpErrorResponse} from '@angular/common/http';
import {MoviesService} from '../movies.service';
import {MessageService} from '../../shared/message.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  // tslint:disable-next-line:component-selector
  selector: '[app-remove]',
  templateUrl: './remove.component.html',
  styleUrls: ['./remove.component.scss'],
  preserveWhitespaces: true
})
export class RemoveComponent implements OnInit {

  @Input()
  movie: {id: string, title: string};

  @Output()
  removed = new EventEmitter<string>();

  @ViewChild('content', { static: true })
  content: ElementRef;

  faTrashAlt = faTrashAlt;

  constructor(private moviesService: MoviesService, private messageService: MessageService, private modalService: NgbModal) { }

  ngOnInit() {
  }

  @HostListener('click')
  open() {
    this.modalService.open(this.content, {ariaLabelledBy: 'modal-basic-title'}).result.then(() => {
      this.remove();
    }, () => {
      // nothing to do
    });
  }

  remove() {
    this.moviesService.remove(this.movie.id)
      .subscribe(() => {
        this.messageService.showSuccess(`Removed "${this.movie.title}"`);
        this.removed.emit(this.movie.id);
      }, (error: HttpErrorResponse) => this.messageService.showError(`Failed to remove movie: ${error.status} - ${error.message}`));
  }

}
