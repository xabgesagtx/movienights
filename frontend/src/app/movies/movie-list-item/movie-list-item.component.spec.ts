import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {MovieListItemComponent} from './movie-list-item.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AuthService} from '../../auth/auth.service';
import {UsernamePipe} from '../../auth/username.pipe';
import {MomentModule} from 'ngx-moment';
import {ImageService} from '../../shared/image.service';

describe('MovieListItemComponent', () => {
  let component: MovieListItemComponent;
  let fixture: ComponentFixture<MovieListItemComponent>;
  const authServiceSpy = jasmine.createSpyObj('AuthService', ['currentUser']);
  const imageServiceSpy = jasmine.createSpyObj('ImageService', ['getPosterThumbnailLink']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        MovieListItemComponent,
        UsernamePipe
      ],
      imports: [
        MomentModule
      ],
      providers: [
        { provide: AuthService, useValue: authServiceSpy },
        { provide: ImageService, useValue: imageServiceSpy },
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieListItemComponent);
    component = fixture.componentInstance;
    component.movie = {
      id: 'id',
      createdAt: '1970-01-01',
      createdBy: { id: 'userId', name: 'user'},
      watchedBy: [{ id: 'userId', name: 'user'}],
      title: 'title',
      year: 1970,
      adult: false,
      poster: 'poster',
      language: 'en',
      originalTitle: 'originalTitle',
      likes: [],
      numberOfComments: 1,
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
