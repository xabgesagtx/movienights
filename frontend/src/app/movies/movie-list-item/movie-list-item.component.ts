import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AuthService} from '../../auth/auth.service';
import {MovieListItem} from '../model/movie-list-item';
import {faComment} from '@fortawesome/free-solid-svg-icons';
import {ImageService} from '../../shared/image.service';

@Component({
  selector: 'app-movie-list-item',
  templateUrl: './movie-list-item.component.html',
  styleUrls: ['./movie-list-item.component.scss'],
  preserveWhitespaces: true
})
export class MovieListItemComponent implements OnInit {

  @Input()
  movie: MovieListItem;
  @Input()
  last: boolean;

  @Output()
  removed = new EventEmitter<string>();

  faComment = faComment;

  constructor(private authService: AuthService, private imageService: ImageService) {
  }

  ngOnInit() {
  }

  get createdAt() {
    return this.movie.createdAt ? new Date(this.movie.createdAt) : null;
  }

  get posterPath() {
    return this.imageService.getPosterThumbnailLink(this.movie.poster);
  }

  get movieClass() {
    if (this.last) {
      return 'movie-item';
    } else {
      return 'movie-item with-border';
    }
  }

  get canRemove() {
    const currentUser = this.authService.currentUser;
    return currentUser.id === this.movie.createdBy.id || currentUser.isAdmin;
  }

  remove() {
    this.removed.emit(this.movie.id);
  }

}
