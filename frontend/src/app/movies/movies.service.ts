import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Movie} from './model/movie';
import {MovieListItem} from './model/movie-list-item';

@Injectable()
export class MoviesService {

  constructor(private httpClient: HttpClient) {
  }

  findById(id: string) {
    return this.httpClient.get<Movie>(`/api/movies/${id}`);
  }

  findAll() {
    return this.httpClient.get<MovieListItem[]>(`/api/movies`);
  }

  remove(id: string) {
    return this.httpClient.delete(`/api/movies/${id}`);
  }

  add(id: number) {
    const params = new HttpParams().append('externalId', id.toString());
    return this.httpClient.post<Movie>('/api/movies/add', null, {params});
  }

  like(id: string) {
    return this.httpClient.post(`/api/movies/${id}/like`, null);
  }

  unlike(id: string) {
    return this.httpClient.delete(`/api/movies/${id}/like`);
  }

  watch(id: string) {
    return this.httpClient.post(`/api/movies/${id}/watch`, null);
  }

  unwatch(id: string) {
    return this.httpClient.delete(`/api/movies/${id}/watch`);
  }
}
