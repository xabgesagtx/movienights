import {TestBed} from '@angular/core/testing';

import {MovieDetailsResolverService} from './movie-details-resolver.service';
import {RouterTestingModule} from '@angular/router/testing';
import {MoviesService} from './movies.service';

describe('MovieDetailsResolverService', () => {
  const moviesServiceSpy = jasmine.createSpyObj('MoviesService', ['findById']);

  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      RouterTestingModule
    ],
    providers: [
      MovieDetailsResolverService,
      { provide: MoviesService, useValue: moviesServiceSpy }
    ]
  }));

  it('should be created', () => {
    const service: MovieDetailsResolverService = TestBed.inject(MovieDetailsResolverService);
    expect(service).toBeTruthy();
  });
});
