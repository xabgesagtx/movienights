import {TestBed} from '@angular/core/testing';

import {MoviesService} from './movies.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('MoviesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule
    ],
    providers: [
      MoviesService
    ]
  }));

  it('should be created', () => {
    const service: MoviesService = TestBed.inject(MoviesService);
    expect(service).toBeTruthy();
  });
});
