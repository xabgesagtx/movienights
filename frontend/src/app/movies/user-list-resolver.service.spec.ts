import {TestBed} from '@angular/core/testing';

import {UserListResolverService} from './user-list-resolver.service';
import {RouterTestingModule} from '@angular/router/testing';
import {UserService} from '../auth/user.service';

describe('UserListResolverService', () => {
  let service: UserListResolverService;

  const userServiceSpy = jasmine.createSpyObj('UserService', ['findAll']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      providers: [
        UserListResolverService,
        {provide: UserService, useValue: userServiceSpy}
      ]
    });
    service = TestBed.inject(UserListResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
