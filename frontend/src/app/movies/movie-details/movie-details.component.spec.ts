import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {MovieDetailsComponent} from './movie-details.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {UsernamePipe} from '../../auth/username.pipe';
import {MomentModule} from 'ngx-moment';
import {AuthService} from '../../auth/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {of} from 'rxjs';
import {ImageService} from '../../shared/image.service';
import {Movie} from '../model/movie';

describe('MovieDetailsComponent', () => {
  const movie: Movie = {
    adult: false,
    countries: [],
    externalId: 1,
    genres: [],
    language: 'en',
    likes: [],
    poster: 'poster',
    videos: [],
    watchedBy: [],
    id: 'id',
    title: 'title',
    createdAt: '1970-01-01',
    createdBy: { id: 'userId', name: 'name'},
    originalTitle: 'originalTitle'
  };

  let component: MovieDetailsComponent;
  let fixture: ComponentFixture<MovieDetailsComponent>;
  let authServiceSpy = jasmine.createSpyObj('AuthService', ['login']);
  const imageServiceSpy = jasmine.createSpyObj('ImageService', ['getPosterLink']);
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

  authServiceSpy = {
    ...authServiceSpy,
    currentUser: {
      id: 'userId',
      name: 'name'
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        MovieDetailsComponent,
        UsernamePipe
      ],
      imports: [
        MomentModule
      ],
      providers: [
        { provide: AuthService, useValue: authServiceSpy },
        { provide: ActivatedRoute, useValue: {
            data: of({movie})
          }
        },
        { provide: ImageService, useValue: imageServiceSpy },
        { provide: Router, useValue: routerSpy }
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
