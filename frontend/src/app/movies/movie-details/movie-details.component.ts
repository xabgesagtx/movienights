import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Movie} from '../model/movie';
import {faExternalLinkAlt} from '@fortawesome/free-solid-svg-icons';
import {AuthService} from '../../auth/auth.service';
import {ImageService} from '../../shared/image.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss'],
  preserveWhitespaces: true
})
export class MovieDetailsComponent implements OnInit {

  movie: Movie;
  faExternalLinkAlt = faExternalLinkAlt;

  constructor(private route: ActivatedRoute, private authService: AuthService, private router: Router, private imageService: ImageService) {
  }

  ngOnInit() {
    this.route.data.subscribe((data: { movie: Movie }) => {
      this.movie = data.movie;
    });
  }

  get createdAt() {
    return this.movie.createdAt ? new Date(this.movie.createdAt) : null;
  }

  get posterPath() {
    return this.imageService.getPosterLink(this.movie.poster);
  }

  get videoIds() {
    return this.movie.videos.map(video => video.id);
  }

  get imdbLink() {
    return `https://www.imdb.com/title/${this.movie.imdbId}`;
  }

  get canRemove() {
    const currentUser = this.authService.currentUser;
    return currentUser.id === this.movie.createdBy.id || currentUser.isAdmin;
  }

  remove() {
    this.router.navigate(['/movies']);
  }

}
