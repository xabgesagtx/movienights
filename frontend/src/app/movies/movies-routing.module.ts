import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MovieListComponent} from './movie-list/movie-list.component';
import {MovieDetailsComponent} from './movie-details/movie-details.component';
import {MovieDetailsResolverService} from './movie-details-resolver.service';
import {LoggedInGuard} from '../auth/logged-in.guard';
import {UserListResolverService} from './user-list-resolver.service';


const routes: Routes = [
  {
    path: 'movies', component: MovieListComponent, canActivate: [LoggedInGuard], resolve: {
      users: UserListResolverService
    }
  },
  {
    path: 'movies/:id', component: MovieDetailsComponent, canActivate: [LoggedInGuard], resolve: {
      movie: MovieDetailsResolverService
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})
  ],
  exports: [RouterModule]
})
export class MoviesRoutingModule {
}
