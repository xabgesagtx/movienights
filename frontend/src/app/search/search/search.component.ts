import {Component, OnDestroy, OnInit} from '@angular/core';
import {SearchService} from '../search.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {MovieSearchResultItem} from '../model/movie-search-result-item';
import {HttpErrorResponse} from '@angular/common/http';
import {MessageService} from '../../shared/message.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {

  searchStarted = false;
  reachedEnd = false;
  loading = false;
  query: string;
  queryForLastSearch: string;
  subscription: Subscription;
  movies: MovieSearchResultItem[] = [];
  page = 0;

  constructor(private searchService: SearchService,
              private messageService: MessageService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.query = params.query;
      if (this.query) {
        this.initSearch();
      }
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onSubmit() {
    if (this.query === this.queryForLastSearch) {
      this.initSearch();
    } else {
      this.router.navigate(['/search'], {
        queryParams: {
          query: this.query
        }
      });
    }
  }

  private initSearch() {
    this.queryForLastSearch = this.query;
    this.movies = [];
    this.reachedEnd = false;
    this.searchStarted = true;
    this.page = 0;
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this.loading = false;
    this.search();
  }

  search() {
    if (!this.reachedEnd && !this.loading) {
      this.loading = true;
      const page = this.page;
      this.subscription = this.searchService.search(this.queryForLastSearch, page)
        .subscribe(
          searchPage => {
            this.reachedEnd = searchPage.page + 1 >= searchPage.numberOfPages;
            this.loading = false;
            this.page = searchPage.page + 1;
            searchPage.movies.forEach(entry => this.movies.push(entry));
            delete this.subscription;
          },
          (error: HttpErrorResponse) => {
            this.messageService.showError(`Search failed: ${error.status}: ${error.message}`);
            this.loading = false;
            delete this.subscription;
          });
    }
  }

}
