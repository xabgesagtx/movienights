import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {SearchComponent} from './search.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {SearchService} from '../search.service';
import {MessageService} from '../../shared/message.service';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule} from '@angular/forms';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  const searchServiceSpy = jasmine.createSpyObj('SearchService', ['search']);
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        SearchComponent
      ],
      imports: [
        FormsModule,
        RouterTestingModule
      ],
      providers: [
        { provide: SearchService, useValue: searchServiceSpy },
        { provide: MessageService, useValue: messageServiceSpy },
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
