import {TestBed} from '@angular/core/testing';

import {MovieSearchDetailsResolverService} from './movie-search-details-resolver.service';
import {SearchService} from './search.service';
import {RouterTestingModule} from '@angular/router/testing';

describe('MovieSearchDetailsResolverService', () => {
  const searchServiceSpy = jasmine.createSpyObj('SearchService', ['details']);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      RouterTestingModule
    ],
    providers: [
      MovieSearchDetailsResolverService,
      { provide: SearchService, useValue: searchServiceSpy}
    ]
  }));

  it('should be created', () => {
    const service: MovieSearchDetailsResolverService = TestBed.inject(MovieSearchDetailsResolverService);
    expect(service).toBeTruthy();
  });
});
