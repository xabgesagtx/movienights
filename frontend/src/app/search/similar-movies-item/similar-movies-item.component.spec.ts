import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {SimilarMoviesItemComponent} from './similar-movies-item.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ImageService} from '../../shared/image.service';

describe('SimilarMoviesItemComponent', () => {
  let component: SimilarMoviesItemComponent;
  let fixture: ComponentFixture<SimilarMoviesItemComponent>;
  const imageServiceSpy = jasmine.createSpyObj('ImageService', ['getPosterLink']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SimilarMoviesItemComponent ],
      providers: [
        { provide: ImageService, useValue: imageServiceSpy }
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimilarMoviesItemComponent);
    component = fixture.componentInstance;
    component.movie = {
      id: 1,
      title: 'title',
      year: 1970,
      poster: 'poster'
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
