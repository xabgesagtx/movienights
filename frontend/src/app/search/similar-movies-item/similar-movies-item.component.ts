import {Component, Input, OnInit} from '@angular/core';
import {SimilarMovie} from '../model/similar-movie';
import {ImageService} from '../../shared/image.service';

@Component({
  selector: 'app-similar-movies-item',
  templateUrl: './similar-movies-item.component.html',
  styleUrls: ['./similar-movies-item.component.scss']
})
export class SimilarMoviesItemComponent implements OnInit {

  @Input()
  movie: SimilarMovie;
  constructor(private imageService: ImageService) { }

  ngOnInit() {
  }

  get posterPath() {
    return this.imageService.getPosterLink(this.movie.poster);
  }

}
