import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {SearchResult} from './model/search-result';
import {MovieSearchDetails} from './model/movie-search-details';
import {SimilarMovie} from './model/similar-movie';

@Injectable()
export class SearchService {

  constructor(private httpClient: HttpClient) { }

  search(query: string, page: number) {
    const params = new HttpParams().append('query', query).append('page', page.toString());
    return this.httpClient.get<SearchResult>('/api/search', { params });
  }

  details(id: number) {
    return this.httpClient.get<MovieSearchDetails>(`/api/search/${id}`);
  }

  similarMovies(id: number) {
    return this.httpClient.get<SimilarMovie[]>(`/api/search/${id}/similarMovies`);
  }

}
