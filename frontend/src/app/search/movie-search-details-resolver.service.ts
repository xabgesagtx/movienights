import {Injectable} from '@angular/core';
import {MovieSearchDetails} from './model/movie-search-details';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {EMPTY, Observable} from 'rxjs';
import {SearchService} from './search.service';
import {HttpErrorResponse} from '@angular/common/http';
import {catchError, map, take} from 'rxjs/operators';

@Injectable()
export class MovieSearchDetailsResolverService implements Resolve<MovieSearchDetails> {

  constructor(private router: Router, private searchService: SearchService) {
  }

  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot):
    Observable<MovieSearchDetails> | Promise<MovieSearchDetails> | MovieSearchDetails {
    const id = parseInt(route.paramMap.get('id'), 10);
    return this.searchService.details(id).pipe(
      take(1),
      map(response => {
        if (response) {
          return response;
        } else {
          this.router.navigate(['/not-found']);
          return null;
        }
      }),
      catchError((err: HttpErrorResponse) => {
        if (err.status === 404) {
          this.router.navigate(['/not-found']);
        } else {
          this.router.navigate(['/some-error']);
        }
        return EMPTY as Observable<MovieSearchDetails>;
      })
    );
  }
}
