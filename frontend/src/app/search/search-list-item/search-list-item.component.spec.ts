import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {SearchListItemComponent} from './search-list-item.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {MessageService} from '../../shared/message.service';
import {MoviesService} from '../../movies/movies.service';
import {ImageService} from '../../shared/image.service';

describe('SearchListItemComponent', () => {
  let component: SearchListItemComponent;
  let fixture: ComponentFixture<SearchListItemComponent>;

  const imageServiceSpy = jasmine.createSpyObj('ImageService', ['getPosterThumbnailLink']);
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError', 'showSuccess']);
  const moviesServiceSpy = jasmine.createSpyObj('MoviesService', ['add']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        SearchListItemComponent
      ],
      providers: [
        { provide: ImageService, useValue: imageServiceSpy },
        { provide: MessageService, useValue: messageServiceSpy },
        { provide: MoviesService, useValue: moviesServiceSpy },
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchListItemComponent);
    component = fixture.componentInstance;
    component.item = {
      adult: false,
      id: 1,
      internalId: null,
      language: 'en',
      originalTitle: 'originalTitle',
      poster: 'poster',
      title: 'title'
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
