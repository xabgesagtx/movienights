import {Component, Input, OnInit} from '@angular/core';
import {MovieSearchResultItem} from '../model/movie-search-result-item';
import {MessageService} from '../../shared/message.service';
import {MoviesService} from '../../movies/movies.service';
import {HttpErrorResponse} from '@angular/common/http';
import {faList, faPlusCircle} from '@fortawesome/free-solid-svg-icons';
import {ImageService} from '../../shared/image.service';

@Component({
  selector: 'app-search-list-item',
  templateUrl: './search-list-item.component.html',
  styleUrls: ['./search-list-item.component.scss'],
  preserveWhitespaces: true
})
export class SearchListItemComponent implements OnInit {

  @Input()
  item: MovieSearchResultItem;
  @Input()
  last: boolean;

  faPlusCircle = faPlusCircle;
  faList = faList;

  constructor(private messageService: MessageService, private moviesService: MoviesService, private imageService: ImageService) {
  }

  ngOnInit() {
  }

  get posterPath() {
    return this.imageService.getPosterThumbnailLink(this.item.poster);
  }

  get searchItemClass() {
    if (this.last) {
      return 'search-item';
    } else {
      return 'search-item with-border';
    }
  }

  add() {
    this.moviesService.add(this.item.id).subscribe(movie => {
      this.item.internalId = movie.id;
      this.messageService.showSuccess(`Added movie ${this.item.title}`);
    }, (error: HttpErrorResponse) => this.messageService.showError(`Failed to add movie: ${error.status} - ${error.message}`));
  }

}
