import {RouterModule, Routes} from '@angular/router';
import {SearchComponent} from './search/search.component';
import {NgModule} from '@angular/core';
import {SearchDetailsComponent} from './search-details/search-details.component';
import {MovieSearchDetailsResolverService} from './movie-search-details-resolver.service';
import {LoggedInGuard} from '../auth/logged-in.guard';

const routes: Routes = [
  { path: 'search', component: SearchComponent, canActivate: [LoggedInGuard] },
  { path: 'search/:id', component: SearchDetailsComponent, canActivate: [LoggedInGuard], resolve: {
      movie: MovieSearchDetailsResolverService
    }}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})
  ],
  exports: [RouterModule]
})
export class SearchRoutingModule {

}
