import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {SearchDetailsComponent} from './search-details.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {MessageService} from '../../shared/message.service';
import {MoviesService} from '../../movies/movies.service';
import {ActivatedRoute} from '@angular/router';
import {of} from 'rxjs';
import {ImageService} from '../../shared/image.service';

describe('SearchDetailsComponent', () => {
  let component: SearchDetailsComponent;
  let fixture: ComponentFixture<SearchDetailsComponent>;

  const movie = {
    id: 1,
    internalId: null,
    title: 'title',
    poster: 'poster',
    adult: false,
    language: 'en',
    originalTitle: 'originalTitle',
    countries: [],
    videos: [],
    genres: []
  };

  const imageServiceSpy = jasmine.createSpyObj('ImageService', ['getPosterLink']);
  const moviesServiceSpy = jasmine.createSpyObj('MoviesService', ['add']);
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError', 'showSuccess']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        SearchDetailsComponent
      ],
      providers: [
        { provide: ImageService, useValue: imageServiceSpy },
        { provide: MoviesService, useValue: moviesServiceSpy },
        { provide: MessageService, useValue: messageServiceSpy },
        { provide: ActivatedRoute, useValue: {
          data: of({movie})
        }}
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
