import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MovieSearchDetails} from '../model/movie-search-details';
import {faExternalLinkAlt, faList, faPlusCircle} from '@fortawesome/free-solid-svg-icons';
import {MoviesService} from '../../movies/movies.service';
import {MessageService} from '../../shared/message.service';
import {HttpErrorResponse} from '@angular/common/http';
import {ImageService} from '../../shared/image.service';

@Component({
  selector: 'app-search-details',
  templateUrl: './search-details.component.html',
  styleUrls: ['./search-details.component.scss'],
  preserveWhitespaces: true
})
export class SearchDetailsComponent implements OnInit {

  movie: MovieSearchDetails;
  faPlusCircle = faPlusCircle;
  faExternalLinkAlt = faExternalLinkAlt;
  faList = faList;
  constructor(private route: ActivatedRoute,
              private moviesService: MoviesService,
              private messageService: MessageService,
              private imageService: ImageService) { }

  ngOnInit() {
    this.route.data.subscribe((data: {movie: MovieSearchDetails}) => {
      this.movie = data.movie;
    });
  }

  get posterPath() {
    return this.imageService.getPosterLink(this.movie.poster);
  }

  get videoIds() {
    return this.movie.videos.map(video => video.id);
  }

  get imdbLink() {
    return `https://www.imdb.com/title/${this.movie.imdbId}`;
  }

  add() {
    this.moviesService.add(this.movie.id)
      .subscribe(movie => {
        this.movie.internalId = movie.id;
        this.messageService.showSuccess('Added movie');
      }, (error: HttpErrorResponse) => this.messageService.showError(`Failed to add movie: ${error.status} - ${error.message}`));
  }
}
