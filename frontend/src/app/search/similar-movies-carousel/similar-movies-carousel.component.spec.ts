import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {SimilarMoviesCarouselComponent} from './similar-movies-carousel.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {SearchService} from '../search.service';
import {MessageService} from '../../shared/message.service';

describe('SimilarMoviesCarouselComponent', () => {
  let component: SimilarMoviesCarouselComponent;
  let fixture: ComponentFixture<SimilarMoviesCarouselComponent>;
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError']);
  const searchServiceSpy = jasmine.createSpyObj('SearchService', ['similarMovies']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        SimilarMoviesCarouselComponent
      ],
      providers: [
        { provide: MessageService, useValue: messageServiceSpy },
        { provide: SearchService, useValue: searchServiceSpy },
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimilarMoviesCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
