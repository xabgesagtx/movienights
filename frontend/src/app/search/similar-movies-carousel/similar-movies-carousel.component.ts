import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {SearchService} from '../search.service';
import {MessageService} from '../../shared/message.service';
import {SimilarMovie} from '../model/similar-movie';
import {HttpErrorResponse} from '@angular/common/http';
import {faArrowLeft, faArrowRight} from '@fortawesome/free-solid-svg-icons';

const ITEM_WIDTH = 280;
const SCROLL_OFFSET_THRESHOLD = 80;

@Component({
  selector: 'app-similar-movies-carousel',
  templateUrl: './similar-movies-carousel.component.html',
  styleUrls: ['./similar-movies-carousel.component.scss']
})
export class SimilarMoviesCarouselComponent implements OnInit {

  private movieIdInternal: number;

  @Input()
  set movieId(movieId: number) {
    if (this.movieIdInternal !== movieId) {
      this.movieIdInternal = movieId;
      this.loadMovies();
    }
  }

  get movieId() {
    return this.movieIdInternal;
  }

  @ViewChild('similarMoviesList')
  similarMoviesList: ElementRef;

  movies: SimilarMovie[] = [];
  loading: boolean;
  faArrowLeft = faArrowLeft;
  faArrowRight = faArrowRight;

  constructor(private searchService: SearchService, private messageService: MessageService) { }

  ngOnInit() {
  }

  loadMovies() {
    this.loading = true;
    this.searchService.similarMovies(this.movieId).subscribe(movies => {
      this.loading = false;
      this.movies = movies;
    }, (error: HttpErrorResponse) => {
      this.loading = false;
      this.messageService.showError(`Could not load similar movies: ${error.status} - ${error.message}`);
    });
  }

  scrollRight() {
    const scrollLeft = this.similarMoviesList.nativeElement.scrollLeft;
    const offsetToLastItem = scrollLeft % ITEM_WIDTH;
    let scrollX: number;
    if (offsetToLastItem > ITEM_WIDTH - SCROLL_OFFSET_THRESHOLD) {
      scrollX = 2 * ITEM_WIDTH - offsetToLastItem;
    } else {
      scrollX = ITEM_WIDTH - offsetToLastItem;
    }
    this.similarMoviesList.nativeElement.scrollBy(scrollX, 0);
  }

  scrollLeft() {
    const scrollLeft = this.similarMoviesList.nativeElement.scrollLeft;
    const offsetToLastItem = scrollLeft % ITEM_WIDTH;
    let scrollX;
    if (offsetToLastItem < SCROLL_OFFSET_THRESHOLD) {
      scrollX = -ITEM_WIDTH - offsetToLastItem;
    } else {
      scrollX = -offsetToLastItem;
    }
    this.similarMoviesList.nativeElement.scrollBy(scrollX, 0);
  }

}
