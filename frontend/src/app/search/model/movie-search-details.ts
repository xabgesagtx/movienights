import {Video} from './video';

export class MovieSearchDetails {
  id: number;
  internalId: string;
  title: string;
  year?: number;
  poster: string;
  description?: string;
  imdbId?: string;
  adult: boolean;
  duration?: number;
  language: string;
  originalTitle: string;
  countries: string[];
  videos: Video[];
  genres: string[];
}
