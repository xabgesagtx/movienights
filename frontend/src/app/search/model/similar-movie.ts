export class SimilarMovie {
  id: number;
  title: string;
  year?: number;
  poster: string;
}
