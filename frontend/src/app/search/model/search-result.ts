import {MovieSearchResultItem} from './movie-search-result-item';

export class SearchResult {
  page: number;
  totalNumberOfResults: number;
  numberOfPages: number;
  movies: MovieSearchResultItem[];
}
