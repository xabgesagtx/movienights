export class MovieSearchResultItem {
  id: number;
  title: string;
  internalId: string;
  year?: number;
  adult: boolean;
  poster: string;
  language: string;
  originalTitle: string;
}
