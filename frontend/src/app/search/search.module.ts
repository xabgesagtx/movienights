import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SearchComponent} from './search/search.component';
import {SearchListItemComponent} from './search-list-item/search-list-item.component';
import {SharedModule} from '../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {SearchDetailsComponent} from './search-details/search-details.component';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {MovieSearchDetailsResolverService} from './movie-search-details-resolver.service';
import {SearchService} from './search.service';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {SimilarMoviesCarouselComponent} from './similar-movies-carousel/similar-movies-carousel.component';
import {SimilarMoviesItemComponent} from './similar-movies-item/similar-movies-item.component';


@NgModule({
  declarations: [
    SearchComponent,
    SearchListItemComponent,
    SearchDetailsComponent,
    SimilarMoviesCarouselComponent,
    SimilarMoviesItemComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    RouterModule,
    InfiniteScrollModule,
    FontAwesomeModule
  ],
  providers: [
    SearchService,
    MovieSearchDetailsResolverService
  ]
})
export class SearchModule {
}
