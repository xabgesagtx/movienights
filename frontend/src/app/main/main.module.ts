import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavigationComponent} from './navigation/navigation.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {AuthModule} from '../auth/auth.module';


@NgModule({
  declarations: [NavigationComponent, NotFoundComponent, HomeComponent],
  imports: [
    CommonModule,
    NgbModule,
    FontAwesomeModule,
    RouterModule,
    AuthModule
  ],
  exports: [
    NotFoundComponent,
    NavigationComponent
  ],
  providers: [
    NotFoundComponent
  ]
})
export class MainModule {
}
