import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SearchRoutingModule} from './search/search-routing.module';
import {NotFoundComponent} from './main/not-found/not-found.component';
import {MainModule} from './main/main.module';
import {MoviesRoutingModule} from './movies/movies-routing.module';
import {HomeComponent} from './main/home/home.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'not-found', component: NotFoundComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'}),
    SearchRoutingModule,
    MoviesRoutingModule,
    MainModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
