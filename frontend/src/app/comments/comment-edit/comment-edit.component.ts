import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CommentService} from '../comment.service';
import {Comment} from '../model/comment';
import {HttpErrorResponse} from '@angular/common/http';
import {MessageService} from '../../shared/message.service';

@Component({
  selector: 'app-comment-edit',
  templateUrl: './comment-edit.component.html',
  styleUrls: ['./comment-edit.component.scss'],
  preserveWhitespaces: true
})
export class CommentEditComponent implements OnInit {

  @Input()
  content = '';

  @Input()
  movieId: string;

  @Input()
  commentId: string;

  @Output()
  commentUpdated = new EventEmitter<Comment>();

  @Output()
  updateCanceled = new EventEmitter<string>();

  constructor(private commentService: CommentService, private messageService: MessageService) { }

  ngOnInit() {
  }

  get canSave() {
    return this.content && this.content.trim() !== '';
  }

  save() {
    this.commentService.update(this.movieId, this.commentId, this.content)
      .subscribe(comment => {
        this.commentUpdated.emit(comment);
        this.content = '';
      }, (error: HttpErrorResponse) => {
        this.messageService.showError(`Failed to update comment: ${error.status} - ${error.message}`);
      });
  }

  cancel() {
    this.updateCanceled.emit(this.commentId);
  }
}
