import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {CommentEditComponent} from './comment-edit.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {CommentService} from '../comment.service';
import {MessageService} from '../../shared/message.service';

describe('CommentEditComponent', () => {
  let component: CommentEditComponent;
  let fixture: ComponentFixture<CommentEditComponent>;
  const commentServiceSpy = jasmine.createSpyObj('CommentService', ['update']);
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        CommentEditComponent
      ],
      providers: [
        { provide: CommentService, useValue: commentServiceSpy },
        { provide: MessageService, useValue: messageServiceSpy },
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
