import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {CommentCreateComponent} from './comment-create.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {CommentService} from '../comment.service';
import {MessageService} from '../../shared/message.service';

describe('CommentCreateComponent', () => {
  let component: CommentCreateComponent;
  let fixture: ComponentFixture<CommentCreateComponent>;
  const commentServiceSpy = jasmine.createSpyObj('CommentService', ['create']);
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentCreateComponent ],
      providers: [
        { provide: CommentService, useValue: commentServiceSpy },
        { provide: MessageService, useValue: messageServiceSpy },
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
