import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Comment} from '../model/comment';
import {CommentService} from '../comment.service';
import {MessageService} from '../../shared/message.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-comment-create',
  templateUrl: './comment-create.component.html',
  styleUrls: ['./comment-create.component.scss']
})
export class CommentCreateComponent implements OnInit {

  @Input()
  movieId: string;

  @Output()
  commentAdded = new EventEmitter<Comment>();

  content = '';

  constructor(private commentService: CommentService, private messageService: MessageService) { }

  ngOnInit() {
  }

  get canSubmit() {
    return this.content && this.content.trim() !== '';
  }

  submit() {
    this.commentService.create(this.movieId, this.content)
      .subscribe(comment => {
        this.commentAdded.emit(comment);
        this.content = '';
      }, (error: HttpErrorResponse) => {
        this.messageService.showError(`Failed to submit comment: ${error.status} - ${error.message}`);
      });
  }

}
