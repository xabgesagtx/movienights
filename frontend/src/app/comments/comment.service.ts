import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CreateCommentRequest} from './model/create-comment-request';
import {UpdateCommentRequest} from './model/update-comment-request';
import {Comment} from './model/comment';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private httpClient: HttpClient) { }

  create(movieId: string, content: string) {
    const request: CreateCommentRequest = { content };
    return this.httpClient.post<Comment>(`/api/movies/${movieId}/comments`, request);
  }

  update(movieId: string, id: string, content: string) {
    const request: UpdateCommentRequest = { content };
    return this.httpClient.patch<Comment>(`/api/movies/${movieId}/comments/${id}`, request);
  }

  findByMovieId(movieId: string) {
    return this.httpClient.get<Comment[]>(`/api/movies/${movieId}/comments`);
  }

  remove(movieId: string, id: string) {
    return this.httpClient.delete(`/api/movies/${movieId}/comments/${id}`);
  }
}
