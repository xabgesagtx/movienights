import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CommentsComponent} from './comments/comments.component';
import {CommentListItemComponent} from './comment-list-item/comment-list-item.component';
import {CommentEditComponent} from './comment-edit/comment-edit.component';
import {SharedModule} from '../shared/shared.module';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {FormsModule} from '@angular/forms';
import {MomentModule} from 'ngx-moment';
import {QuillModule} from 'ngx-quill';
import {CommentCreateComponent} from './comment-create/comment-create.component';
import {AuthModule} from '../auth/auth.module';


@NgModule({
  declarations: [CommentsComponent, CommentListItemComponent, CommentEditComponent, CommentCreateComponent],
  exports: [
    CommentsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FontAwesomeModule,
    FormsModule,
    MomentModule,
    QuillModule,
    AuthModule
  ]
})
export class CommentsModule { }
