import {User} from './user';

export class Comment {
  id: string;
  createdBy: User;
  createdAt: string;
  modifiedAt: string;
  content: string;
  movieId: string;
}
