import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {CommentListItemComponent} from './comment-list-item.component';
import {NO_ERRORS_SCHEMA, Pipe, PipeTransform} from '@angular/core';
import {CommentService} from '../comment.service';
import {MessageService} from '../../shared/message.service';
import {MomentModule} from 'ngx-moment';
import {AuthService} from '../../auth/auth.service';

@Pipe({
  name: 'username'
})
class MockUsernamePipe implements PipeTransform {

  transform(value: {username: string}): any {
    return value.username;
  }

}

describe('CommentListItemComponent', () => {
  let component: CommentListItemComponent;
  let fixture: ComponentFixture<CommentListItemComponent>;
  let authServiceSpy = jasmine.createSpyObj('AuthService', ['login']);
  const commentServiceSpy = jasmine.createSpyObj('CommentService', ['remove']);
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError']);

  authServiceSpy = {
    ...authServiceSpy,
    currentUser: {id: 'userId', name: 'name', isAdmin: false}
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        CommentListItemComponent,
        MockUsernamePipe
      ],
      imports: [
        MomentModule
      ],
      providers: [
        { provide: AuthService, useValue: authServiceSpy },
        { provide: CommentService, useValue: commentServiceSpy },
        { provide: MessageService, useValue: messageServiceSpy },
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentListItemComponent);
    component = fixture.componentInstance;
    component.comment = {
      id: 'id',
      movieId: 'movieId',
      content: 'bla',
      createdAt: '1970-01-01',
      createdBy: { id: 'userId', name: 'name'},
      modifiedAt: '1970-01-01',
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
