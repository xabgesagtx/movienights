import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CommentService} from '../comment.service';
import {Comment} from '../model/comment';
import {HttpErrorResponse} from '@angular/common/http';
import {MessageService} from '../../shared/message.service';
import {AuthService} from '../../auth/auth.service';
import {faEdit, faPen, faTrashAlt} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-comment-list-item',
  templateUrl: './comment-list-item.component.html',
  styleUrls: ['./comment-list-item.component.scss'],
  preserveWhitespaces: true
})
export class CommentListItemComponent implements OnInit {

  @Input()
  comment: Comment;

  @Output()
  commentRemoved = new EventEmitter<string>();

  faTrashAlt = faTrashAlt;
  faPen = faPen;
  faEdit = faEdit;
  editing = false;

  constructor(private commentService: CommentService, private messageService: MessageService, private authService: AuthService) { }

  ngOnInit() {
  }

  get showLastChange() {
    return this.comment.modifiedAt !== this.comment.createdAt;
  }

  get canEdit() {
    const currentUser = this.authService.currentUser;
    return currentUser.id === this.comment.createdBy.id;
  }

  get canRemove() {
    const currentUser = this.authService.currentUser;
    return currentUser.id === this.comment.createdBy.id || currentUser.isAdmin;
  }

  get createdAt() {
    return this.comment.createdAt ? new Date(this.comment.createdAt) : null;
  }

  get modifiedAt() {
    return this.comment.modifiedAt ? new Date(this.comment.modifiedAt) : null;
  }

  startEditing() {
    this.editing = true;
  }

  stopEditing() {
    this.editing = false;
  }

  commentUpdated(comment: Comment) {
    this.comment = comment;
    this.editing = false;
  }

  remove() {
    this.commentService.remove(this.comment.movieId, this.comment.id).subscribe(() => {
      this.commentRemoved.emit(this.comment.id);
    }, (error: HttpErrorResponse) => this.messageService.showError(`Failed to remove comment: ${error.status} - ${error.message}`));
  }

}
