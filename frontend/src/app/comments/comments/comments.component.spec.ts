import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {CommentsComponent} from './comments.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {CommentService} from '../comment.service';
import {MessageService} from '../../shared/message.service';
import {of} from 'rxjs';

describe('CommentsComponent', () => {
  let component: CommentsComponent;
  let fixture: ComponentFixture<CommentsComponent>;
  const commentServiceSpy = jasmine.createSpyObj('CommentService', {
    findByMovieId: of([])
  });
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        CommentsComponent
      ],
      providers: [
        {provide: CommentService, useValue: commentServiceSpy},
        {provide: MessageService, useValue: messageServiceSpy},
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
