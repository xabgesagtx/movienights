import {Component, Input, OnInit} from '@angular/core';
import {Comment} from '../model/comment';
import {CommentService} from '../comment.service';
import {MessageService} from '../../shared/message.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {

  @Input()
  movieId: string;

  loading: boolean;
  comments: Comment[] = [];

  constructor(private commentService: CommentService, private messageService: MessageService) { }

  ngOnInit() {
    this.loading = true;
    this.commentService.findByMovieId(this.movieId)
      .subscribe(comments => {
        this.comments = comments;
        this.loading = false;
      }, (error: HttpErrorResponse) => {
        this.messageService.showError(`Failed loading comments: ${error.status} - ${error.message}`);
        this.loading = false;
      });
  }

  removeComment(id: string) {
    const index = this.comments.findIndex(comment => comment.id === id);
    if (index !== -1) {
      this.comments.splice(index, 1);
    }
  }

  addComment(comment: Comment) {
    this.comments.push(comment);
  }
}
