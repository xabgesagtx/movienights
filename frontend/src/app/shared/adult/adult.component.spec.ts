import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {AdultComponent} from './adult.component';

describe('AdultComponent', () => {
  let component: AdultComponent;
  let fixture: ComponentFixture<AdultComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
