import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-adult',
  templateUrl: './adult.component.html',
  styleUrls: ['./adult.component.scss']
})
export class AdultComponent implements OnInit {

  @Input()
  adult: boolean;

  constructor() { }

  ngOnInit() {
  }

}
