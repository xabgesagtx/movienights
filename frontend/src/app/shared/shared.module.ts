import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoadingSpinnerComponent} from './loading-spinner/loading-spinner.component';
import {YoutubeVideoComponent} from './youtube-video/youtube-video.component';
import {MessageService} from './message.service';
import {AdultComponent} from './adult/adult.component';
import {LanguageComponent} from './language/language.component';
import {GenresComponent} from './genres/genres.component';
import {YoutubeVideosComponent} from './youtube-videos/youtube-videos.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {SafeUrlPipe} from './safe-url.pipe';
import {ImageService} from './image.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';


@NgModule({
  declarations: [
    LoadingSpinnerComponent,
    YoutubeVideoComponent,
    AdultComponent,
    LanguageComponent,
    GenresComponent,
    YoutubeVideosComponent,
    SafeUrlPipe
  ],
  exports: [
    LoadingSpinnerComponent,
    YoutubeVideoComponent,
    AdultComponent,
    LanguageComponent,
    GenresComponent,
    YoutubeVideosComponent
  ],
  imports: [
    CommonModule,
    MatSnackBarModule,
    NgbModule
  ],
  providers: [
    MessageService,
    ImageService
  ]
})
export class SharedModule { }
