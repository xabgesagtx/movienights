import {Injectable} from '@angular/core';

@Injectable()
export class ImageService {

  constructor() {
  }

  private static getImageLink(path: string, type: string, size: string) {
    return `https://tmdb-images.canceledsystems.com/image?path=${path}&type=${type}&size=${size}`;
  }

  getPosterLink(path: string) {
    const type = 'POSTER';
    const size = 'MEDIUM';
    return ImageService.getImageLink(path, type, size);
  }

  getPosterThumbnailLink(path: string) {
    const type = 'POSTER';
    const size = 'EXTRASMALL';
    return ImageService.getImageLink(path, type, size);
  }

}
