import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable()
export class MessageService {

  constructor(private snackBar: MatSnackBar) { }

  showError(message: string) {
    this.snackBar.open(message, null, {
      duration: 5000,
      panelClass: 'text-danger'
    });
  }

  showSuccess(message: string) {
    this.snackBar.open(message, null, {
      duration: 5000
    });
  }

}
