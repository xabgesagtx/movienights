import {TestBed} from '@angular/core/testing';

import {MessageService} from './message.service';
import {MatSnackBar} from '@angular/material/snack-bar';

describe('MessageService', () => {
  const matSnackBarSpy = jasmine.createSpyObj('MatSnackBar', ['open']);

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      MessageService,
      { provide: MatSnackBar, useValue: matSnackBarSpy }
    ]
  }));

  it('should be created', () => {
    const service: MessageService = TestBed.inject(MessageService);
    expect(service).toBeTruthy();
  });
});
