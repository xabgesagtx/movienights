import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {YoutubeVideosComponent} from './youtube-videos.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('YoutubeVideosComponent', () => {
  let component: YoutubeVideosComponent;
  let fixture: ComponentFixture<YoutubeVideosComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        YoutubeVideosComponent
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YoutubeVideosComponent);
    component = fixture.componentInstance;
    component.videoIds = [ 'id1', 'id2' ];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
