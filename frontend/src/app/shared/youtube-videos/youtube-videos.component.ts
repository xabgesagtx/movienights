import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-youtube-videos',
  templateUrl: './youtube-videos.component.html',
  styleUrls: ['./youtube-videos.component.scss']
})
export class YoutubeVideosComponent implements OnInit {

  @Input()
  videoIds: string[];

  hideVideos = true;

  constructor() { }

  ngOnInit() {
  }

  get dropdownText() {
    const prefix = this.hideVideos ? 'More' : 'Less';
    return prefix + ' videos';
  }

  get dropdownClass() {
    return this.hideVideos ? '' : 'dropup';
  }

  toggleShowMoreVideos() {
    this.hideVideos = !this.hideVideos;
  }

}
