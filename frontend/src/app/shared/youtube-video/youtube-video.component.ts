import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-youtube',
  templateUrl: './youtube-video.component.html',
  styleUrls: ['./youtube-video.component.scss']
})
export class YoutubeVideoComponent implements OnInit {

  @Input()
  id: string;

  constructor() {}

  ngOnInit() {
  }

  get url() {
    return `https://www.youtube.com/embed/${this.id}`;
  }

}
