import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {YoutubeVideoComponent} from './youtube-video.component';
import {SafeUrlPipe} from '../safe-url.pipe';
import {BrowserModule} from '@angular/platform-browser';

describe('YoutubeVideoComponent', () => {
  let component: YoutubeVideoComponent;
  let fixture: ComponentFixture<YoutubeVideoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        YoutubeVideoComponent,
        SafeUrlPipe
      ],
      imports: [
        BrowserModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YoutubeVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
