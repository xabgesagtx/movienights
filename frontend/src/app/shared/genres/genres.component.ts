import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-genres',
  templateUrl: './genres.component.html',
  styleUrls: ['./genres.component.scss']
})
export class GenresComponent implements OnInit {

  @Input()
  genres: string[];

  constructor() { }

  ngOnInit() {
  }

}
