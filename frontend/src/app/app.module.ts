import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SearchModule} from './search/search.module';
import {HttpClientModule} from '@angular/common/http';
import {MainModule} from './main/main.module';
import {SharedModule} from './shared/shared.module';
import {MoviesModule} from './movies/movies.module';
import {AuthModule} from './auth/auth.module';
import {QuillModule} from 'ngx-quill';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SearchModule,
    MoviesModule,
    MainModule,
    HttpClientModule,
    SharedModule,
    AuthModule,
    QuillModule.forRoot({
      modules: {
        toolbar: [
          ['bold', 'italic', 'underline', 'strike'],
          ['blockquote'],
          [{ list: 'ordered'}, { list: 'bullet' }],
          [{ script: 'sub'}, { script: 'super' }],
          ['clean'],
          ['link', 'image', 'video']
        ]
      }
    }),
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
