package com.canceledsystems.movienights.comments.service

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test

class ContentCleanerTest {

    private val cleaner = ContentCleaner()

    @Test
    fun `clean removes invalid tag and keeps allowed one`() {
        val actual = cleaner.clean("<strong>keep</strong><span>remove</span>")
        val expected = "<strong>keep</strong>remove"
        assertThat(actual, equalTo(expected))
    }

    @Test
    fun `clean removes attributes from allowed tag strong`() {
        val actual = cleaner.clean("<strong class=\"remove\">keep</strong>")
        val expected = "<strong>keep</strong>"
        assertThat(actual, equalTo(expected))
    }

    @Test
    fun `clean removes not allowed class from iframe`() {
        val actual = cleaner.clean("<iframe class=\"remove\" src=\"https://www.youtube.com/embed/test\">keep</iframe>")
        val expected = "<iframe src=\"https://www.youtube.com/embed/test\">keep</iframe>"
        assertThat(actual, equalTo(expected))
    }

    @Test
    fun `clean keeps allowed class from iframe`() {
        val actual = cleaner.clean("<iframe class=\"ql-video\" src=\"https://www.youtube.com/embed/test\">keep</iframe>")
        val expected = "<iframe class=\"ql-video\" src=\"https://www.youtube.com/embed/test\">keep</iframe>"
        assertThat(actual, equalTo(expected))
    }

    @Test
    fun `clean removes iframe without src`() {
        val actual = cleaner.clean("<iframe>remove</iframe>")
        val expected = ""
        assertThat(actual, equalTo(expected))
    }

    @Test
    fun `clean removes iframe with invalid src`() {
        val actual = cleaner.clean("<iframe src=\"https://example.com\">remove</iframe>")
        val expected = ""
        assertThat(actual, equalTo(expected))
    }

    @Test
    fun `clean removes https image`() {
        val actual = cleaner.clean("<img src=\"https://example.com/image.jpeg\">")
        val expected = ""
        assertThat(actual, equalTo(expected))
    }

    @Test
    fun `clean keeps base64 image`() {
        val actual = cleaner.clean("<img src=\"data:image/jpeg;base64,test\">")
        val expected = "<img src=\"data:image/jpeg;base64,test\">"
        assertThat(actual, equalTo(expected))
    }

}