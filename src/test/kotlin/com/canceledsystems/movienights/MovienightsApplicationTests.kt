package com.canceledsystems.movienights

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(properties = ["tmdb.cache.path=/tmp/imagecache", "tmdb.apiKey=test"])
class MovienightsApplicationTests {

    @Test
    fun contextLoads() {
    }

}
