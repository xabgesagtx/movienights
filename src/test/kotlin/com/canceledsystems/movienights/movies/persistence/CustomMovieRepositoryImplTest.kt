package com.canceledsystems.movienights.movies.persistence

import com.nhaarman.mockitokotlin2.whenever
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest
import org.springframework.boot.test.mock.mockito.MockBean
import java.time.Clock
import java.time.Instant
import java.time.temporal.ChronoUnit

@DataMongoTest
class CustomMovieRepositoryImplTest {

    @MockBean
    lateinit var clock: Clock

    @Autowired
    lateinit var customRepo: CustomMovieRepositoryImpl

    @Autowired
    lateinit var repo: MovieRepository

    private val id = "id"

    private val userId1 = "userId1"
    private val userId2 = "userId2"

    private val instant = Instant.now().truncatedTo(ChronoUnit.MILLIS)

    @BeforeEach
    fun setUp() {
        repo.deleteAll()
        val movieDocument = MovieDocument(id = id,
                genres = emptyList(),
                title = "title",
                externalId = 1,
                originalTitle = "originalTitle",
                duration = 0,
                adult = false,
                poster = "poster",
                imdbId = "imdbId",
                language = "en",
                year = 2001,
                videos = emptyList(),
                countries = emptyList(),
                description = "description")
        repo.save(movieDocument)

        whenever(clock.instant()).thenReturn(instant)
    }

    @Test
    fun `watch marks movie as watched`() {
        customRepo.watch(id, userId1)

        val actual = repo.findById(id)
        assertTrue(actual.isPresent)
        assertThat(actual.get().watchedBy, equalTo(listOf(userId1)))
    }

    @Test
    fun `unwatch marks previously marked as watched movie as unwatched`() {
        customRepo.watch(id, userId1)
        customRepo.unwatch(id, userId1)

        val actual = repo.findById(id)
        assertTrue(actual.isPresent)
        assertThat(actual.get().watchedBy, equalTo(emptyList()))
    }

    @Test
    fun `like without previous like`() {
        customRepo.like(id, userId1)
        val actual = repo.findById(id)
        assertTrue(actual.isPresent)
        assertThat(actual.get().likes, equalTo(listOf(userId1)))
    }

    @Test
    fun `like with previous like by same user`() {
        customRepo.like(id, userId1)
        customRepo.like(id, userId1)
        val actual = repo.findById(id)
        assertTrue(actual.isPresent)
        assertThat(actual.get().likes, equalTo(listOf(userId1)))
    }

    @Test
    fun `like with previous like by other user`() {
        customRepo.like(id, userId1)
        customRepo.like(id, userId2)
        val actual = repo.findById(id)
        assertTrue(actual.isPresent)
        assertThat(actual.get().likes, equalTo(listOf(userId1, userId2)))
    }

    @Test
    fun `unlike with previous like by same user`() {
        customRepo.like(id, userId1)
        customRepo.unlike(id, userId1)
        val actual = repo.findById(id)
        assertTrue(actual.isPresent)
        assertThat(actual.get().likes, equalTo(emptyList()))
    }

    @Test
    fun `unlike without previous like`() {
        customRepo.unlike(id, userId1)
        val actual = repo.findById(id)
        assertTrue(actual.isPresent)
        assertThat(actual.get().likes, equalTo(emptyList()))
    }

    @Test
    fun `unlike with previous like by other user`() {
        customRepo.like(id, userId1)
        customRepo.unlike(id, userId2)
        val actual = repo.findById(id)
        assertTrue(actual.isPresent)
        assertThat(actual.get().likes, equalTo(listOf(userId1)))
    }
}