package com.canceledsystems.movienights

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity

@SpringBootApplication
@EnableGlobalMethodSecurity(prePostEnabled = true)
class MovienightsApplication

fun main(args: Array<String>) {
	runApplication<MovienightsApplication>(*args)
}
