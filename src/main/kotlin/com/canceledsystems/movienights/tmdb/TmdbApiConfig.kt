package com.canceledsystems.movienights.tmdb

import com.omertron.themoviedbapi.TheMovieDbApi
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class TmdbApiConfig {

    @Bean
    fun theMovieDbApi(properties: TmdbApiProperties): TheMovieDbApi {
        return TheMovieDbApi(properties.apiKey)
    }

}