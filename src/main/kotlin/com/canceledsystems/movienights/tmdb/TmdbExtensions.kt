package com.canceledsystems.movienights.tmdb

fun String.year(): Int? {
    return this.substringBefore("-").toIntOrNull()
}