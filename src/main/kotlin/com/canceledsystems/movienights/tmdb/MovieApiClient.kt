package com.canceledsystems.movienights.tmdb

import com.omertron.themoviedbapi.TheMovieDbApi
import com.omertron.themoviedbapi.enumeration.SearchType
import com.omertron.themoviedbapi.model.config.Configuration
import com.omertron.themoviedbapi.model.media.Video
import com.omertron.themoviedbapi.model.movie.MovieInfo
import com.omertron.themoviedbapi.results.ResultList
import io.github.resilience4j.ratelimiter.annotation.RateLimiter
import org.slf4j.LoggerFactory
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Component

@Component
@RateLimiter(name = "tmdb")
class MovieApiClient(private val tmdbApi: TheMovieDbApi) {

    private val log = LoggerFactory.getLogger(MovieApiClient::class.java)

    fun search(query: String, page: Int): ResultList<MovieInfo> {
        return tmdbApi.searchMovie(query, page, null, true, null, null, SearchType.NGRAM)
    }

    fun details(id: Int): MovieInfo {
        return tmdbApi.getMovieInfo(id, null)
    }

    fun videos(movieId: Int): ResultList<Video> {
        return tmdbApi.getMovieVideos(movieId, null)
    }

    @Cacheable("tmdbConfiguration")
    fun getConfiguration(): Configuration {
        log.info("Loading tmdb configuration")
        return tmdbApi.configuration
    }

    fun similarMovies(id: Int): ResultList<MovieInfo> {
        return tmdbApi.getSimilarMovies(id, 1, null)
    }

}