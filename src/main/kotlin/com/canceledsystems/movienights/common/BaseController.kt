package com.canceledsystems.movienights.common

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
class BaseController {

    @RequestMapping("/search/**", "/movies/**", "/not-found")
    fun index(): String {
        return "forward:/index.html"
    }
}