package com.canceledsystems.movienights.common

import com.canceledsystems.movienights.users.persistence.UserDocument
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.convert.converter.Converter
import org.springframework.data.mongodb.core.convert.MongoCustomConversions


@Configuration
class MongoConfig {

    @Bean
    fun mongoCustomConversions(): MongoCustomConversions {
        return MongoCustomConversions(listOf(movieUserConverter(), commentUserConverter()))
    }

    private fun movieUserConverter(): Converter<UserDocument, com.canceledsystems.movienights.movies.persistence.UserProjection> {
        return Converter { source -> com.canceledsystems.movienights.movies.persistence.UserProjection(source.id, source.name) }
    }

    private fun commentUserConverter(): Converter<UserDocument, com.canceledsystems.movienights.comments.persistence.UserProjection> {
        return Converter { source -> com.canceledsystems.movienights.comments.persistence.UserProjection(source.id, source.name) }
    }
}