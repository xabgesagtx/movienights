package com.canceledsystems.movienights.common

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.domain.AuditorAware
import org.springframework.data.mongodb.config.EnableMongoAuditing
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.data.repository.query.SecurityEvaluationContextExtension
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken
import java.util.*


@Configuration
@EnableMongoAuditing(auditorAwareRef = "auditorProvider")
class AuditingConfig {

    @Bean
    fun auditorProvider(): AuditorAware<String> {
        return AuditorAware {
            if (SecurityContextHolder.getContext().authentication != null) {
                val oauth2 = SecurityContextHolder.getContext().authentication as JwtAuthenticationToken
                val loggedInUserId = oauth2.token.claims["sub"].toString()
                Optional.of(loggedInUserId)
            } else {
                Optional.empty()
            }
        }
    }

    @Bean
    fun securityEvaluationContextExtension(): SecurityEvaluationContextExtension {
        return SecurityEvaluationContextExtension()
    }
}