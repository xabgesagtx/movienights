package com.canceledsystems.movienights.comments.persistence

data class UserProjection(val id: String, val name: String)