package com.canceledsystems.movienights.comments.persistence

data class MovieCommentCount(val id: String, val count: Long)