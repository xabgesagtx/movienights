package com.canceledsystems.movienights.comments.persistence

interface CustomCommentRepository {

    fun updateContent(id: String, userId: String, content: String)

    fun findProjectedById(id: String): CommentProjection?

    fun findProjectedByMovieId(movieId: String): List<CommentProjection>

    fun getMovieCommentStats(movieIds: Collection<String>): Map<String,Long>
}