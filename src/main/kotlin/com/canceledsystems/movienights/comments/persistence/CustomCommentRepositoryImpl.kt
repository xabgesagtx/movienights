package com.canceledsystems.movienights.comments.persistence

import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.aggregation.Aggregation.*
import org.springframework.data.mongodb.core.aggregation.AggregationResults
import org.springframework.data.mongodb.core.aggregation.LookupOperation
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Criteria.where
import org.springframework.data.mongodb.core.query.Query.query
import org.springframework.data.mongodb.core.query.Update
import org.springframework.stereotype.Repository
import java.time.Clock

@Repository
class CustomCommentRepositoryImpl(private val mongoTemplate: MongoTemplate, private val clock: Clock) : CustomCommentRepository {

    override fun getMovieCommentStats(movieIds: Collection<String>): Map<String, Long> {
        val match = match(where("movieId").`in`(movieIds))
        val group = group("movieId").count().`as`("count")
        val aggregation = newAggregation(match, group)
        return mongoTemplate.aggregate(aggregation, "commentDocument", MovieCommentCount::class.java)
                .mappedResults
                .associateBy(MovieCommentCount::id, MovieCommentCount::count)
    }

    override fun findProjectedById(id: String): CommentProjection? {
        val criteria = where("_id").`is`(id)
        return findProjected(criteria).uniqueMappedResult
    }

    override fun findProjectedByMovieId(movieId: String): List<CommentProjection> {
        val criteria = where("movieId").`is`(movieId)
        return findProjected(criteria).mappedResults
    }

    override fun updateContent(id: String, userId: String, content: String) {
        val update = Update().set("content", content)
                .set("modifiedBy", userId)
                .set("modifiedAt", clock.instant())
        mongoTemplate.updateFirst(query(where("id").`is`(id)), update, CommentDocument::class.java)
    }


    private fun findProjected(criteria: Criteria): AggregationResults<CommentProjection> {
        val createdByLookup = LookupOperation.newLookup()
                .from("userDocument")
                .localField("createdBy")
                .foreignField("_id")
                .`as`("createdBy")
        val modifiedByLookup = LookupOperation.newLookup()
                .from("userDocument")
                .localField("modifiedBy")
                .foreignField("_id")
                .`as`("modifiedBy")
        val sort = sort(Sort.by(Sort.Order.asc("createdAt")))
        val match = match(criteria)
        val aggregation = newAggregation(match, createdByLookup, modifiedByLookup, sort)
        return mongoTemplate.aggregate(aggregation, "commentDocument", CommentProjection::class.java)
    }
}