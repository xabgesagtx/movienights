package com.canceledsystems.movienights.comments.persistence

import org.springframework.data.annotation.*
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document(collection = "commentDocument")
data class CommentDocument(@Id var id: String? = null,
                           @CreatedBy val createdBy: String? = null,
                           @CreatedDate val createdAt: Instant? = null,
                           @LastModifiedBy val modifiedBy: String? = null,
                           @LastModifiedDate val modifiedAt: Instant? = null,
                           val movieId: String,
                           val content: String)