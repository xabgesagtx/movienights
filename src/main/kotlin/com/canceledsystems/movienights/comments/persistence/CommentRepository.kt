package com.canceledsystems.movienights.comments.persistence

import org.springframework.data.repository.CrudRepository

interface CommentRepository : CrudRepository<CommentDocument, String>, CustomCommentRepository {
}