package com.canceledsystems.movienights.comments.persistence

import java.time.Instant

data class CommentProjection(val id: String,
                             val createdBy: UserProjection,
                             val createdAt: Instant,
                             val modifiedBy: UserProjection,
                             val modifiedAt: Instant,
                             val movieId: String,
                             val content: String)