package com.canceledsystems.movienights.comments.service

import org.jsoup.Jsoup
import org.springframework.stereotype.Service

@Service
class ContentCleaner {

    private val whitelist = RichTextEditorWhitelist()

    fun clean(content: String): String {
        val cleanContent = Jsoup.clean(content, whitelist)
        val doc = Jsoup.parse(cleanContent)
        doc.select("iframe:not([src]), img:not([src])").remove()
        return doc.select("body").html()
    }
}