package com.canceledsystems.movienights.comments.service

import java.time.Instant

data class Comment(val id: String,
                   val createdBy: User,
                   val createdAt: Instant,
                   val modifiedBy: User,
                   val modifiedAt: Instant,
                   val movieId: String,
                   val content: String)