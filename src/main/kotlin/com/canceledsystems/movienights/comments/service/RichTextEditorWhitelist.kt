package com.canceledsystems.movienights.comments.service

import org.jsoup.nodes.Attribute
import org.jsoup.nodes.Element
import org.jsoup.safety.Whitelist

class RichTextEditorWhitelist: Whitelist() {
    init {
        addTags("a", "blockquote", "em", "img", "p", "iframe", "li", "ol", "s", "strong", "sub", "sup", "u", "ul")
        addAttributes("a", "href", "rel", "target")
        addAttributes("img", "src")
        addAttributes("iframe", "class", "frameborder", "allowfullscreen")
        addProtocols("iframe", "src", "https")
        addProtocols("img", "src", "data")
    }

    override fun isSafeAttribute(tagName: String, el: Element, attr: Attribute): Boolean {
        return when {
            "iframe" == tagName && "src" == attr.key -> attr.value.startsWith("https://www.youtube.com/embed/")
            "iframe" == tagName && "class" == attr.key -> attr.value == "ql-video"
            else -> super.isSafeAttribute(tagName, el, attr)
        }
    }
}