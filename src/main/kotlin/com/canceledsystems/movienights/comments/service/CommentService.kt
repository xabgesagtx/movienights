package com.canceledsystems.movienights.comments.service

import com.canceledsystems.movienights.comments.persistence.CommentDocument
import com.canceledsystems.movienights.comments.persistence.CommentProjection
import com.canceledsystems.movienights.comments.persistence.CommentRepository
import com.canceledsystems.movienights.comments.persistence.UserProjection
import com.canceledsystems.movienights.users.service.UserService
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service

@Service
class CommentService(private val repo: CommentRepository,
                     private val userService: UserService,
                     private val contentCleaner: ContentCleaner) {

    fun findByMovieId(movieId: String) = repo.findProjectedByMovieId(movieId).map { it.toComment() }

    fun findById(id: String) = repo.findProjectedById(id)?.toComment()

    @PreAuthorize("hasAuthority('SCOPE_movienights_admin') or #comment.createdBy.id == authentication?.token.claims['sub']")
    fun update(comment: Comment): Comment {
        userService.getCurrentUserId()?.let { repo.updateContent(comment.id, it, comment.content.clean())} ?: throw IllegalStateException("Could not retrieve user")
        return repo.findProjectedById(comment.id)!!.toComment()
    }

    @PreAuthorize("hasAuthority('SCOPE_movienights_admin') or #comment.createdBy.id == authentication?.token.claims['sub']")
    fun remove(comment: Comment) {
        repo.deleteById(comment.id)
    }

    fun create(movieId: String, content: String): Comment {
        val document = CommentDocument(movieId = movieId, content = content.clean())
        val id = repo.save(document).id!!
        return repo.findProjectedById(id)!!.toComment()
    }

    fun getMovieCommentStats(movieIds: List<String>): Map<String, Long> {
        return repo.getMovieCommentStats(movieIds)
    }

    fun String.clean(): String {
        return contentCleaner.clean(this)
    }
}

private fun CommentProjection.toComment() = Comment(id = id,
        createdAt = createdAt,
        createdBy = createdBy.toUser(),
        modifiedAt = modifiedAt,
        modifiedBy = modifiedBy.toUser(),
        movieId = movieId,
        content = content)

private fun UserProjection.toUser() = User(id, name)