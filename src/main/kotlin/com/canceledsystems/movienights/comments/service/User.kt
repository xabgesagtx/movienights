package com.canceledsystems.movienights.comments.service

data class User(val id: String,
                val name: String)