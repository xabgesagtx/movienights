package com.canceledsystems.movienights.comments.rest

data class UserDTO(val id: String,
                   val name: String)