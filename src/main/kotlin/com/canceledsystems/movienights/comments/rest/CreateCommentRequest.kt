package com.canceledsystems.movienights.comments.rest

data class CreateCommentRequest(val content: String)