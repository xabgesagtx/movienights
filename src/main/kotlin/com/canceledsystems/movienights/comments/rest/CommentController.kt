package com.canceledsystems.movienights.comments.rest

import com.canceledsystems.movienights.comments.service.Comment
import com.canceledsystems.movienights.comments.service.CommentService
import com.canceledsystems.movienights.comments.service.User
import com.canceledsystems.movienights.movies.service.MovieService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("api/movies")
class CommentController(private val movieService: MovieService,
                        private val commentService: CommentService) {

    @GetMapping("{movieId}/comments")
    fun findByMovieId(@PathVariable("movieId") movieId: String): List<CommentDTO> {
        checkMovieExists(movieId)
        return commentService.findByMovieId(movieId).map { it.toDTO() }
    }

    @PostMapping("{movieId}/comments")
    fun create(@PathVariable("movieId") movieId: String, @RequestBody request: CreateCommentRequest): CommentDTO {
        checkMovieExists(movieId)
        return commentService.create(movieId, request.content).toDTO()
    }

    @PatchMapping("{movieId}/comments/{id}")
    fun update(@PathVariable("movieId") movieId: String, @PathVariable("id") id: String, @RequestBody request: UpdateCommentRequest): CommentDTO {
        checkMovieExists(movieId)
        return commentService.findById(id)?.let { commentService.update(it.copy(content = request.content)).toDTO() } ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find comment with id $id")
    }

    @DeleteMapping("{movieId}/comments/{id}")
    fun remove(@PathVariable("movieId") movieId: String, @PathVariable("id") id: String) {
        checkMovieExists(movieId)
        commentService.findById(id)?.let { commentService.remove(it) } ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find comment with id $id")
    }

    private fun checkMovieExists(movieId: String) {
        if (!movieService.exists(movieId)) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find movie with id $movieId")
        }
    }
}

private fun Comment.toDTO() = CommentDTO(id = id,
        createdAt = createdAt,
        createdBy = createdBy.toDTO(),
        modifiedAt = modifiedAt,
        modifiedBy = modifiedBy.toDTO(),
        movieId = movieId,
        content = content)

private fun User.toDTO() = UserDTO(id, name)
