package com.canceledsystems.movienights.comments.rest

data class UpdateCommentRequest(val content: String)