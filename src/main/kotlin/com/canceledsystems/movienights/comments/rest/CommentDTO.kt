package com.canceledsystems.movienights.comments.rest

import java.time.Instant

data class CommentDTO(val id: String,
                      val createdBy: UserDTO,
                      val createdAt: Instant,
                      val modifiedBy: UserDTO,
                      val modifiedAt: Instant,
                      val movieId: String,
                      val content: String)