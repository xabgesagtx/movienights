package com.canceledsystems.movienights.users.service

data class User(val id: String,
                val name: String,
                val email: String)