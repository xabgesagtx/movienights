package com.canceledsystems.movienights.users.service

import com.canceledsystems.movienights.users.persistence.UserDocument
import com.canceledsystems.movienights.users.persistence.UserRepository
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken
import org.springframework.stereotype.Service

@Service
class UserService(private val repo: UserRepository) {

    fun register(user: User) {
        // TODO: Replace with call to profile endpoint from auth0
        repo.findById(user.id)
                .map { it.copy(name = user.name, email = user.email) }
                .orElseGet { UserDocument(id = user.id, name = user.name, email = user.email)}
                .let { repo.save(it) }
    }

    fun getCurrentUserId(): String? {
        return if (SecurityContextHolder.getContext().authentication != null) {
            val oauth2 = SecurityContextHolder.getContext().authentication as JwtAuthenticationToken
            return oauth2.token.claims["sub"]?.toString()
        } else {
            null
        }
    }

    fun findAll(): List<User> {
        return repo.findAll()
            .toList()
            .sortedBy { it.name }
            .map { it.toUser() }
    }
}

private fun UserDocument.toUser(): User = User(id = id, name = name, email = email)