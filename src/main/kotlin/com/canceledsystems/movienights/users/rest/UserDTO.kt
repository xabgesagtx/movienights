package com.canceledsystems.movienights.users.rest

data class UserDTO(val id: String, val name: String)
