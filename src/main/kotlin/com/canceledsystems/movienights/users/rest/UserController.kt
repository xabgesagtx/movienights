package com.canceledsystems.movienights.users.rest

import com.canceledsystems.movienights.users.service.User
import com.canceledsystems.movienights.users.service.UserService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/users")
class UserController(private val userService: UserService) {

    @GetMapping
    fun findAll() = userService.findAll().map { it.toDTO() }

    @PostMapping("register")
    fun register(@RequestBody request: RegisterUserRequest) = userService.register(request.toUser())
}

private fun User.toDTO() = UserDTO(id = id, name = name)

private fun RegisterUserRequest.toUser() = User(id = id, name = name, email = email);
