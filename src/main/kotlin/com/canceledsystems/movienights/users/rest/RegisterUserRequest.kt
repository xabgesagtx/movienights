package com.canceledsystems.movienights.users.rest

data class RegisterUserRequest(val id: String, val name: String, val email: String)