package com.canceledsystems.movienights.users.persistence

import org.springframework.data.repository.CrudRepository


interface UserRepository: CrudRepository<UserDocument, String>