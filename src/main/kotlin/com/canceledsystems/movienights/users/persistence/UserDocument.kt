package com.canceledsystems.movienights.users.persistence

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.annotation.Version
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document(collection = "userDocument")
data class UserDocument(@Id val id: String,
                        val name: String,
                        val email: String,
                        @CreatedDate var createdAt: Instant? = null,
                        @LastModifiedDate var updatedAt: Instant? = null,
                        @Version val version: Long? = null)