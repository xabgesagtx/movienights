package com.canceledsystems.movienights.movies.service

import com.canceledsystems.movienights.movies.persistence.*
import com.canceledsystems.movienights.tmdb.MovieApiClient
import com.canceledsystems.movienights.tmdb.year
import com.canceledsystems.movienights.users.service.UserService
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service

@Service
class MovieService(private val repo: MovieRepository,
                   private val movieApiClient: MovieApiClient,
                   private val userService: UserService) {

    fun findAll(): List<Movie> = this.repo.findAllProjected().map { it.toMovie() }

    fun add(externalId: Int): Movie {
        val movie = getMovieFromApi(externalId)
        val id = repo.save(movie).id
        return findById(id!!)!!
    }

    fun existsByExternalId(externalId: Int): Boolean = repo.existsByExternalId(externalId)

    @PreAuthorize("hasAuthority('SCOPE_movienights_admin') or #movie.createdBy.id == authentication?.token.claims['sub']")
    fun remove(movie: Movie) = repo.deleteById(movie.id)

    fun findById(id: String): Movie? = repo.findProjectedById(id)?.toMovie()

    fun findByExternalId(id: Int): Movie? = repo.findProjectedByExternalId(id)?.toMovie()

    fun findByExternalIds(externalIds: List<Int>): List<Movie> = repo.findAllProjectedByExternalIdIn(externalIds).map { it.toMovie() }

    private fun getMovieFromApi(externalId: Int): MovieDocument {
        val details = movieApiClient.details(externalId)
        val videos = movieApiClient.videos(externalId).results.filter { it.type == "Trailer" && it.site == "YouTube" }
                .map { VideoNode(id = it.key, site = it.site) }
        val countries = details.productionCountries?.map { it.name } ?: emptyList()
        val genres = details.genres?.map { it.name } ?: emptyList()
        return MovieDocument(externalId = details.id,
                title = details.title,
                year = details.releaseDate?.year(),
                poster = details.posterPath,
                language = details.originalLanguage,
                originalTitle = details.originalTitle,
                imdbId = details.imdbID,
                countries = countries,
                description = details.overview,
                videos = videos,
                duration = details.runtime,
                adult = details.isAdult,
                genres = genres)
    }

    fun exists(id: String) = repo.existsById(id)
    
    fun like(id: String) =
            userService.getCurrentUserId()?.let { repo.like(id, it) } ?: throw IllegalStateException("Could not retrieve user")

    fun unlike(id: String) =
            userService.getCurrentUserId()?.let { repo.unlike(id, it) } ?: throw IllegalStateException("Could not retrieve user")

    fun watch(id: String) {
        userService.getCurrentUserId()?.let { repo.watch(id, it) } ?: throw IllegalStateException("Could not retrieve user")
    }

    fun unwatch(id: String) {
        userService.getCurrentUserId()?.let { repo.unwatch(id, it) } ?: throw IllegalStateException("Could not retrieve user")
    }

}

private fun MovieProjection.toMovie(): Movie = Movie(id = id,
        externalId = externalId,
        title = title,
        year = year,
        createdAt = createdAt,
        createdBy = createdBy.toUser(),
        modifiedAt = modifiedAt,
        modifiedBy = modifiedBy.toUser(),
        watchedBy = watchedBy.map { it.toUser() },
        description = description,
        poster = poster,
        originalTitle = originalTitle,
        language = language,
        imdbId = imdbId,
        countries = countries,
        videos = videos.map { Video(it.id, it.site) },
        duration = duration,
        adult = adult,
        genres = genres,
        likes = likes.map { it.toUser() })

private fun UserProjection.toUser() = User(id, name)


