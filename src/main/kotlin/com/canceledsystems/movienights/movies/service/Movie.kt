package com.canceledsystems.movienights.movies.service

import java.time.Instant

data class Movie(val id: String,
                 val externalId: Int,
                 val createdAt: Instant,
                 val createdBy: User,
                 val modifiedAt: Instant,
                 val modifiedBy: User,
                 val title: String,
                 val poster: String?,
                 val description: String?,
                 val year: Int?,
                 val originalTitle: String,
                 val countries: List<String>,
                 val videos: List<Video>,
                 val language: String,
                 val duration: Int?,
                 val adult: Boolean,
                 val imdbId: String?,
                 val genres: List<String>,
                 val likes: List<User>,
                 val watchedBy: List<User>)