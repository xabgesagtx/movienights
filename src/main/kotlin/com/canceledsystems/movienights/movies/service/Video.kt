package com.canceledsystems.movienights.movies.service

data class Video(val id: String,
                 val site: String)