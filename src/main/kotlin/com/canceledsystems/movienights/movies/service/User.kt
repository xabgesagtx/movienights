package com.canceledsystems.movienights.movies.service

data class User(val id: String, val name: String)