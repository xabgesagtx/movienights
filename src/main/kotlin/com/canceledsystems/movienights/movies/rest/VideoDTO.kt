package com.canceledsystems.movienights.movies.rest

data class VideoDTO(val id: String,
                    val site: String)