package com.canceledsystems.movienights.movies.rest

data class UserDTO(val id: String, val name: String)