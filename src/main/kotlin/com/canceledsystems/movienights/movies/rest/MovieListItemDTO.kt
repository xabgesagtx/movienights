package com.canceledsystems.movienights.movies.rest

import java.time.Instant

data class MovieListItemDTO(
    val id: String,
    val createdAt: Instant,
    val createdBy: UserDTO,
    val title: String,
    val year: Int?,
    val adult: Boolean,
    val poster: String?,
    val language: String,
    val originalTitle: String?,
    val likes: List<UserDTO>,
    val watchedBy: List<UserDTO>,
    val numberOfComments: Long
)