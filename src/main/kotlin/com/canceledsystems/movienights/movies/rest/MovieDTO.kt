package com.canceledsystems.movienights.movies.rest

import com.canceledsystems.movienights.movies.service.User
import java.time.Instant

data class MovieDTO(
    val id: String,
    val createdAt: Instant,
    val createdBy: UserDTO,
    val modifiedAt: Instant,
    val modifiedBy: UserDTO,
    val title: String,
    val poster: String?,
    val description: String?,
    val year: Int?,
    val originalTitle: String,
    val countries: List<String>,
    val videos: List<VideoDTO>,
    val language: String,
    val imdbId: String?,
    val genres: List<String>,
    val likes: List<UserDTO>,
    val duration: Int?,
    val adult: Boolean,
    val watchedBy: List<User>
)