package com.canceledsystems.movienights.movies.rest

import com.canceledsystems.movienights.comments.service.CommentService
import com.canceledsystems.movienights.movies.service.Movie
import com.canceledsystems.movienights.movies.service.MovieService
import com.canceledsystems.movienights.movies.service.User
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("api/movies")
class MovieController(private val movieService: MovieService,
                      private val commentService: CommentService) {

    @GetMapping("")
    fun findAll(): List<MovieListItemDTO> {
        val movies = movieService.findAll()
        val movieIds = movies.map { it.id }
        val movieCommentStats = commentService.getMovieCommentStats(movieIds)
        return movies.map { it.toListItemDTO(movieCommentStats) }
    }

    @PostMapping("add")
    fun add(@RequestParam("externalId") externalId: Int): Movie {
        return if (movieService.existsByExternalId(externalId)) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Move with external id $externalId already exists")
        } else {
            movieService.add(externalId)
        }
    }

    @DeleteMapping("{id}")
    fun remove(@PathVariable("id") id: String) {
        val movie = movieService.findById(id) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find movie with id $id")
        movieService.remove(movie)
    }

    @GetMapping("{id}")
    fun findById(@PathVariable("id") id: String): MovieDTO {
        return movieService.findById(id)?.toDTO()
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find movie with id $id")
    }

    @PostMapping("{id}/like")
    fun like(@PathVariable("id") id: String) {
        if (movieService.exists(id)) {
            movieService.like(id)
        } else {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find movie with id $id")
        }
    }

    @DeleteMapping("{id}/like")
    fun unlike(@PathVariable("id") id: String) {
        if (movieService.exists(id)) {
            movieService.unlike(id)
        } else {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find movie with id $id")
        }
    }

    @PostMapping("{id}/watch")
    fun watch(@PathVariable("id") id: String) {
        if (movieService.exists(id)) {
            movieService.watch(id)
        } else {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find movie with id $id")
        }
    }

    @DeleteMapping("{id}/watch")
    fun unwatch(@PathVariable("id") id: String) {
        if (movieService.exists(id)) {
            movieService.unwatch(id)
        } else {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find movie with id $id")
        }
    }
}

private fun Movie.toListItemDTO(movieCommentStats: Map<String, Long>) = MovieListItemDTO(id = id,
            title = title,
            year = year,
            createdAt = createdAt,
            createdBy = createdBy.toDTO(),
            poster = poster,
            originalTitle = originalTitle,
            language = language,
            likes = likes.map { it.toDTO() },
            watchedBy = watchedBy.map { it.toDTO() },
            adult = adult,
            numberOfComments = movieCommentStats[id] ?: 0)

private fun Movie.toDTO(): MovieDTO = MovieDTO(id = id,
        title = title,
        year = year,
        createdAt = createdAt,
        createdBy = createdBy.toDTO(),
        modifiedAt = modifiedAt,
        modifiedBy = modifiedBy.toDTO(),
        watchedBy = watchedBy,
        description = description,
        poster = poster,
        originalTitle = originalTitle,
        language = language,
        imdbId = imdbId,
        countries = countries,
        videos = videos.map { VideoDTO(it.id, it.site) },
        genres = genres,
        likes = likes.map { it.toDTO() },
        duration = duration,
        adult = adult)

private fun User.toDTO(): UserDTO = UserDTO(id = id, name = name)
