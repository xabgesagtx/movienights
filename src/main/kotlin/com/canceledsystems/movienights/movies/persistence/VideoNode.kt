package com.canceledsystems.movienights.movies.persistence

data class VideoNode(val id: String,
                     val site: String)