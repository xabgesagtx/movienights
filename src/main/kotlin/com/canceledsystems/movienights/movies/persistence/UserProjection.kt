package com.canceledsystems.movienights.movies.persistence

import org.springframework.data.annotation.Id

data class UserProjection(@Id val id: String, val name: String)