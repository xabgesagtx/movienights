package com.canceledsystems.movienights.movies.persistence

data class VideoNodeProjection(val id: String,
                               val site: String)