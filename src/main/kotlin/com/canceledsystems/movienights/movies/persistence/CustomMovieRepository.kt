package com.canceledsystems.movienights.movies.persistence

interface CustomMovieRepository {

    fun like(id: String, userId: String)
    fun unlike(id: String, userId: String)
    fun watch(id: String, userId: String)
    fun unwatch(id: String, userId: String)
    fun findAllProjected(): List<MovieProjection>
    fun findProjectedById(id: String): MovieProjection?
    fun findProjectedByExternalId(externalId: Int): MovieProjection?
    fun findAllProjectedByExternalIdIn(externalIds: List<Int>): List<MovieProjection>
}