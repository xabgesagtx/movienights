package com.canceledsystems.movienights.movies.persistence

import org.springframework.data.domain.Sort.Order.asc
import org.springframework.data.domain.Sort.by
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.aggregation.Aggregation
import org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation
import org.springframework.data.mongodb.core.aggregation.Aggregation.sort
import org.springframework.data.mongodb.core.aggregation.AggregationResults
import org.springframework.data.mongodb.core.aggregation.LookupOperation.newLookup
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Criteria.where
import org.springframework.data.mongodb.core.query.Query.query
import org.springframework.data.mongodb.core.query.Update
import org.springframework.stereotype.Repository

@Repository
class CustomMovieRepositoryImpl(private val mongoTemplate: MongoTemplate) : CustomMovieRepository {

    override fun watch(id: String, userId: String) {
        mongoTemplate.updateFirst(query(where("id").`is`(id)), Update().addToSet("watchedBy", userId), MovieDocument::class.java)
    }

    override fun unwatch(id: String, userId: String) {
        mongoTemplate.updateFirst(query(where("id").`is`(id)), Update().pull("watchedBy", userId), MovieDocument::class.java)
    }

    override fun like(id: String, userId: String) {
        mongoTemplate.updateFirst(query(where("id").`is`(id)), Update().addToSet("likes", userId), MovieDocument::class.java)
    }

    override fun unlike(id: String, userId: String) {
        mongoTemplate.updateFirst(query(where("id").`is`(id)), Update().pull("likes", userId), MovieDocument::class.java)
    }

    override fun findAllProjected(): List<MovieProjection> {
        return findProjected(null).mappedResults
    }

    override fun findAllProjectedByExternalIdIn(externalIds: List<Int>): List<MovieProjection> {
        val criteria = where("externalId").`in`(externalIds)
        return findProjected(criteria).mappedResults
    }

    override fun findProjectedByExternalId(externalId: Int): MovieProjection? {
        val criteria = where("externalId").`is`(externalId)
        return findProjected(criteria).uniqueMappedResult
    }

    override fun findProjectedById(id: String): MovieProjection? {
        val criteria = where("_id").`is`(id)
        return findProjected(criteria).uniqueMappedResult
    }

    private fun findProjected(criteria: Criteria?): AggregationResults<MovieProjection> {
        val createdByLookup = newLookup()
                .from("userDocument")
                .localField("createdBy")
                .foreignField("_id")
                .`as`("createdBy")
        val modifiedByLookup = newLookup()
                .from("userDocument")
                .localField("modifiedBy")
                .foreignField("_id")
                .`as`("modifiedBy")
        val likesLookup = newLookup()
                .from("userDocument")
                .localField("likes")
                .foreignField("_id")
                .`as`("likes")
        val watchedByLookup = newLookup()
            .from("userDocument")
            .localField("watchedBy")
            .foreignField("_id")
            .`as`("watchedBy")
        val sort = sort(by(asc("title")))
        val operations = mutableListOf(createdByLookup, modifiedByLookup, likesLookup, watchedByLookup, sort)
        criteria?.let { operations.add(0, Aggregation.match(it)) }
        val aggregation = newAggregation(operations)
        return mongoTemplate.aggregate(aggregation, "movieDocument", MovieProjection::class.java)
    }
}