package com.canceledsystems.movienights.movies.persistence

import java.time.Instant

data class MovieProjection(val id: String,
                           val externalId: Int,
                           val createdBy: UserProjection,
                           val createdAt: Instant,
                           val modifiedBy: UserProjection,
                           val modifiedAt: Instant,
                           val title: String,
                           val description: String?,
                           val poster: String?,
                           val year: Int?,
                           val originalTitle: String,
                           val countries: List<String>,
                           val videos: List<VideoNodeProjection>,
                           val language: String,
                           val duration: Int?,
                           val adult: Boolean,
                           val imdbId: String?,
                           val genres: List<String>,
                           val likes: List<UserProjection> = emptyList(),
                           val watchedBy: List<UserProjection> = emptyList())