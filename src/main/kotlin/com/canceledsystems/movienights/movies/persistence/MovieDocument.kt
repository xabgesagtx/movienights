package com.canceledsystems.movienights.movies.persistence

import org.springframework.data.annotation.*
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document(collection = "movieDocument")
data class MovieDocument(@Id val id: String? = null,
                         @Indexed val externalId: Int,
                         @CreatedBy val createdBy: String? = null,
                         @CreatedDate val createdAt: Instant? = null,
                         @LastModifiedBy val modifiedBy: String? = null,
                         @LastModifiedDate val modifiedAt: Instant? = null,
                         @Version val version: Int? = null,
                         val title: String,
                         val description: String?,
                         val poster: String?,
                         val year: Int?,
                         val originalTitle: String,
                         val countries: List<String>,
                         val videos: List<VideoNode>,
                         val language: String,
                         val duration: Int?,
                         val adult: Boolean,
                         val imdbId: String?,
                         val genres: List<String>,
                         val watchedBy: List<String> = emptyList(),
                         val likes: List<String> = emptyList())