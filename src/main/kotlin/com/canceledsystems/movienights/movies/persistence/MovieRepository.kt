package com.canceledsystems.movienights.movies.persistence

import org.springframework.data.repository.CrudRepository

interface MovieRepository: CrudRepository<MovieDocument, String>, CustomMovieRepository {

    fun existsByExternalId(externalId: Int): Boolean

}