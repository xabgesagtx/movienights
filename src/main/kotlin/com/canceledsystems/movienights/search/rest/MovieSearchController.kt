package com.canceledsystems.movienights.search.rest

import com.canceledsystems.movienights.search.service.MovieSearchDetails
import com.canceledsystems.movienights.search.service.MovieSearchResultItem
import com.canceledsystems.movienights.search.service.MovieSearchService
import com.canceledsystems.movienights.search.service.SimilarMovie
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/search")
class MovieSearchController(private val searchService: MovieSearchService) {

    @GetMapping("{id}")
    fun details(@PathVariable("id") id: Int): MovieSearchDetailsDTO {
        return searchService.details(id).toDTO()
    }
    @GetMapping("{id}/similarMovies")
    fun similarMovies(@PathVariable("id") id: Int): List<SimilarMovieDTO> {
        return searchService.similarMovies(id).map { it.toDTO() }
    }


    @GetMapping("")
    fun search(@RequestParam("query") query: String, @RequestParam("page") page: Int): MovieSearchResultDTO {
        val searchResult = searchService.search(query, page)
        val movies = searchResult.movies.map(this::toDTO)
        return MovieSearchResultDTO(page = searchResult.page,
                numberOfPages = searchResult.numberOfPages,
                totalNumberOfResults = searchResult.totalNumberOfResults,
                movies = movies)
    }

    private fun toDTO(item: MovieSearchResultItem): MovieSearchResultItemDTO {
        return MovieSearchResultItemDTO(id = item.id,
                internalId = item.internalId,
                title = item.title,
                year = item.year,
                poster = item.poster,
                language = item.language,
                originalTitle = item.originalTitle,
                adult = item.adult)
    }
}

private fun SimilarMovie.toDTO() = SimilarMovieDTO(id = id,
        title = title,
        year = year,
        poster = poster)

private fun MovieSearchDetails.toDTO(): MovieSearchDetailsDTO {
    return MovieSearchDetailsDTO(id = id,
            internalId = internalId,
            title = title,
            poster = poster,
            year = year,
            language = language,
            originalTitle = originalTitle,
            description = description,
            imdbId = imdbId,
            countries = countries,
            videos = videos.map { VideoDTO(id = it.id, site = it.site) },
            adult = adult,
            duration = duration,
            genres = genres)
}
