package com.canceledsystems.movienights.search.rest

data class SimilarMovieDTO(val id: Int,
                           val title: String,
                           val year: Int?,
                           val poster: String)