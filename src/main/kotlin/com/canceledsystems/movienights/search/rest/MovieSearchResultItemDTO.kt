package com.canceledsystems.movienights.search.rest

data class MovieSearchResultItemDTO(val id: Int,
                                    val internalId: String?,
                                    val title: String,
                                    val year: Int?,
                                    val poster: String?,
                                    val language: String,
                                    val originalTitle: String,
                                    val adult: Boolean)