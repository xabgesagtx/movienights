package com.canceledsystems.movienights.search.rest

data class MovieSearchDetailsDTO(val id: Int,
                                 val internalId: String?,
                                 val title: String,
                                 val poster: String?,
                                 val year: Int?,
                                 val language: String,
                                 val originalTitle: String,
                                 val description: String?,
                                 val imdbId: String?,
                                 val countries: List<String>,
                                 val videos: List<VideoDTO>,
                                 val duration: Int?,
                                 val adult: Boolean,
                                 val genres: List<String>)