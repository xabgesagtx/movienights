package com.canceledsystems.movienights.search.rest

data class MovieSearchResultDTO(val page: Int,
                                val totalNumberOfResults: Int,
                                val numberOfPages: Int,
                                val movies: List<MovieSearchResultItemDTO>)