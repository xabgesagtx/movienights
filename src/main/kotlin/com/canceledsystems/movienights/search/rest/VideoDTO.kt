package com.canceledsystems.movienights.search.rest

data class VideoDTO(val id: String,
                    val site: String)