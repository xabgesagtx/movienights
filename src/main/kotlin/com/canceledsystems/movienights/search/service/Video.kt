package com.canceledsystems.movienights.search.service

data class Video(val id: String,
                 val site: String)