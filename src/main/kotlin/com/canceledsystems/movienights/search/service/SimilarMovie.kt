package com.canceledsystems.movienights.search.service

data class SimilarMovie(val id: Int,
                        val title: String,
                        val year: Int?,
                        val poster: String)