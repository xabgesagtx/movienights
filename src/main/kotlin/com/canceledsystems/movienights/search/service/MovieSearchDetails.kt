package com.canceledsystems.movienights.search.service

data class MovieSearchDetails(val id: Int,
                              val internalId: String?,
                              val title: String,
                              val poster: String?,
                              val year: Int?,
                              val language: String,
                              val originalTitle: String,
                              val description: String?,
                              val imdbId: String?,
                              val countries: List<String>,
                              val videos: List<Video>,
                              val duration: Int?,
                              val genres: List<String>,
                              val adult: Boolean)