package com.canceledsystems.movienights.search.service

data class MovieSearchResultItem(val id: Int,
                                 val internalId: String?,
                                 val title: String,
                                 val poster: String?,
                                 val year: Int?,
                                 val language: String,
                                 val originalTitle: String,
                                 val adult: Boolean)