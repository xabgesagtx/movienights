package com.canceledsystems.movienights.search.service

data class MovieSearchResult(val page: Int,
                             val totalNumberOfResults: Int,
                             val numberOfPages: Int,
                             val movies: List<MovieSearchResultItem>)