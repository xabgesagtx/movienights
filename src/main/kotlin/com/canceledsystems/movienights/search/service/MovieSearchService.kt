package com.canceledsystems.movienights.search.service

import com.canceledsystems.movienights.movies.service.MovieService
import com.canceledsystems.movienights.tmdb.MovieApiClient
import com.canceledsystems.movienights.tmdb.year
import com.omertron.themoviedbapi.model.movie.MovieInfo
import org.springframework.stereotype.Service

@Service
class MovieSearchService(private val movieApiClient: MovieApiClient, private val movieService: MovieService) {

    fun details(id: Int): MovieSearchDetails {
        val details = movieApiClient.details(id)
        val videos = movieApiClient.videos(id).results.filter { it.type == "Trailer" && it.site == "YouTube" }
                .map { Video(id = it.key, site = it.site) }
        val countries = details.productionCountries?.map { it.name } ?: emptyList()
        val internalId = movieService.findByExternalId(id)?.id
        val genres = details.genres?.map { it.name } ?: emptyList()
        return MovieSearchDetails(id = details.id,
                internalId = internalId,
                title = details.title,
                year = details.releaseDate?.year(),
                poster = details.posterPath,
                language = details.originalLanguage,
                originalTitle = details.originalTitle,
                imdbId = details.imdbID,
                countries = countries,
                description = details.overview,
                videos = videos,
                adult = details.isAdult,
                duration = details.runtime,
                genres = genres)
    }

    fun search(query: String, page: Int): MovieSearchResult {
        val searchResult = movieApiClient.search(query, page + 1)
        val ids = searchResult.results.map { it.id }
        val idToInternalId = movieService.findByExternalIds(ids).associateBy({ it.externalId }, { it.id })
        val movies = searchResult.results.map { toSearchResultItem(it, idToInternalId[it.id]) }
        return MovieSearchResult(page = searchResult.page - 1,
                totalNumberOfResults = searchResult.totalResults,
                numberOfPages = searchResult.totalPages,
                movies = movies)
    }

    fun similarMovies(id: Int): List<SimilarMovie> {
        return movieApiClient.similarMovies(id).results
                .mapNotNull { it.toSimilarMovie() }
    }

    private fun MovieInfo.toSimilarMovie(): SimilarMovie? {
        return posterPath?.let {
            SimilarMovie(id = id,
                    title = title,
                    year = releaseDate?.year(),
                    poster = posterPath)
        }
    }

    private fun toSearchResultItem(movieInfo: MovieInfo, internalId: String?): MovieSearchResultItem {
        return MovieSearchResultItem(id = movieInfo.id,
                internalId = internalId,
                title = movieInfo.title,
                year = movieInfo.releaseDate?.year(),
                poster = movieInfo.posterPath,
                language = movieInfo.originalLanguage,
                originalTitle = movieInfo.originalTitle,
                adult = movieInfo.isAdult)
    }

}

