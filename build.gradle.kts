import com.moowork.gradle.node.npm.NpmTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.bundling.BootBuildImage
import org.springframework.boot.gradle.tasks.bundling.BootJar

plugins {
	id("com.github.node-gradle.node") version "2.2.4"
	id("org.springframework.boot") version "2.4.1"
	id("io.spring.dependency-management") version "1.0.10.RELEASE"
	kotlin("jvm") version "1.4.21"
	kotlin("plugin.spring") version "1.4.21"
}

java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
}

node {
	version = "14.15.0"
	npmVersion = "6.14.9"
	nodeModulesDir = file("${projectDir}/frontend")
	download = true
}

tasks.register<NpmTask>("runLint") {
	setWorkingDir(file("${projectDir}/frontend"))
	setArgs(listOf("run", "lint"))
	dependsOn(":npmInstall")
}

tasks.register<NpmTask>("runKarma") {
	setWorkingDir(file("${projectDir}/frontend"))
	setArgs(listOf("run", "test-headless"))
	dependsOn(":npmInstall")
}

tasks.register<NpmTask>("buildAngular") {
	setWorkingDir(file("${projectDir}/frontend"))
	setArgs(listOf("run", "build"))
	dependsOn(":npmInstall")
}

tasks.getByName<BootJar>("bootJar") {
	if (!project.hasProperty("skipNode")) {
		dependsOn(":buildAngular")
	}
	from("frontend/dist/frontend") {
		into("public")
	}
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-security")
	implementation("org.springframework.security:spring-security-data")
	implementation("org.springframework.boot:spring-boot-starter-data-mongodb")
	implementation("org.springframework.security:spring-security-oauth2-resource-server")
	implementation("org.springframework.security:spring-security-oauth2-jose")
	implementation("io.github.resilience4j:resilience4j-spring-boot2:1.5.0")
	implementation("org.springframework.boot:spring-boot-starter-actuator")
	implementation("io.micrometer:micrometer-registry-prometheus")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("com.omertron:themoviedbapi:4.3")
	implementation("commons-io:commons-io:2.6")
	implementation("org.jsoup:jsoup:1.12.1")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0")
	testImplementation("de.flapdoodle.embed:de.flapdoodle.embed.mongo")
}

tasks.withType<BootBuildImage> {
	docker {
		publishRegistry {
			username = project.properties["dockerUsername"]?.toString()
			password = project.properties["dockerPassword"]?.toString()
		}
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}


tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}
