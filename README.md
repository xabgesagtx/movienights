# Movienights


[![pipeline status](https://gitlab.com/xabgesagtx/movienights/badges/master/pipeline.svg)](https://gitlab.com/xabgesagtx/movienights/commits/master)

![Movienights](screenshots/screenshot.png "Movienights screenshot")

A website for coordinating movie nights with friends.

Features:
* search movie in the [The Movie Database API](https://developers.themoviedb.org/3/)
* add them as suggestions to watch at your next movie night
* see movie details, posters and trailers
* like and comment on movie suggestions
* special permission to admin users to remove others movie suggestions and comments
* OAuth2 based authentication

Technologies used:
* backend
    * spring boot based application
    * written in kotlin
    * mongodb for persistence
    * using spring security for authentication
    * built with gradle
* frontend
    * written in typescript
    * using angular as main framework
    * bootstrap for layout
    * built with NPM

## Dependencies

* java 11 for building backend, node 14+ for frontend
* docker or java 11 for running
* OAuth2 identity provider for authentication

## Build

Movienights comes with a gradle wrapper to build the application as jar

```bash
./gradlew build
```

Gradle is configured to not only build the backend code but also build the typescript frontend code and package them together in a single jar file.

## Configuration

As movienights depends on external systems, there is some configuration needed.

The easiest way for a spring boot application is to create an application.yml

```yaml
# TMDb API configuration
tmdb:
    apiKey: YOUR_TMDB_API_KEY
# MongoDB configuration
spring:
  data:
    mongodb:
      database: DB_NAME
      host: HOSTNAME
      username: USERNAME
      password: PASSWORD
# OAuth2 configuration
security:
    oauth2:
      resourceserver:
        jwt:
          jwk-set-uri: URL_TO_THE_CERTIFICATES_OF_OAUTH2_PROVIDER
```

__NOTE__: The oauth2 provider and client id is currently hard coded in angular and needs to be changed if you want to use a different provider.

## Development

To run in development you have to provide at least the Google API key.

For convenience there is a `docker-compose.yml` to start a local mongo instance. This means you can leave out the mongo configuraiton.

Run the following commands to prepare local development:
```bash
docker-compose up -d
cd frontend
npm install
npm run start
```

Afterwards start the kotlin backend in your favorite IDE providing the necessary configuration outlined above.

## Run in production

### Jar

You can either build the application yourself (see above) and run the jar:

```bash
java -jar build/libs/movienights.jar
```

Make sure you put the application.yml with the required configurations in the folder where you run this command.

### Docker

Alternatively, you can use the docker image from gitlab:

```bash
docker run -p "8080:8080" -v PATH_TO_APPLICATION_YML:/application.yml registry.gitlab.com/xabgesagtx/movienights
```